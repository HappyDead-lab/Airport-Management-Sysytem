package controller.Ticket;

import Main.Main;
import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.*;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;

public class BuyTicketPageController implements Initializable {
    private @FXML
    Button exitBTN;
    private @FXML
    Button buyBTN;
    private @FXML
    Button cancelBTN;
    private @FXML
    TableView<Ticket> ticketTableView;
    private @FXML
    TableColumn<Ticket, String> id;
    private @FXML
    TableColumn<Ticket, String> price;
    private @FXML
    TableColumn<Ticket, String> priceCancelling;
    private @FXML
    TableColumn<Ticket, String> flightID;

    public void setEnterParticularFlightID(boolean enterParticularFlightID) {
        isEnterParticularFlightID = enterParticularFlightID;
    }

    //this variable save flight id
    private boolean isEnterParticularFlightID;

    public void setAirplaneID(String saveAirplaneID) {
        this.saveAirplaneID = saveAirplaneID;
    }

    //this variable save flight id
    private String saveAirplaneID;

    //this variable for save input username that passenger login with it
    private String saveUsername;

    public void setFlightID(String flightID) {
        this.saveFlightID = flightID;
    }

    //this variable save flight id
    private String saveFlightID;

    public void setSaveUsername(String saveUsername) {
        this.saveUsername = saveUsername;
    }

    //set on action for exit button
    public void setOnActionExitBTN(ActionEvent event) {
        //get current window
        Stage stage = (Stage) exitBTN.getScene().getWindow();
        stage.close();
    }


    //set on action for buy button
    public void setOnActionBuyBTN(ActionEvent event) {
        //get current window
        Stage stage = (Stage) buyBTN.getScene().getWindow();
        //this variable check credit is enough or not
        boolean enoughCredit = false;
        int price = 0;
        int credit = 0;
        //this variables check flight capacity
        int numberOfSoldTicket = 0;
        int capacity = 0;
        boolean enoughCapacity = false;
        int indexOfPassenger = 0;
        //these variable check that passenger already bought this ticket or didn't
        boolean isBought = false;
        //this variables save date to flight, time to flight and taken time
        LocalDate dateToFlight = null;
        LocalTime timeToFlight = null;
        boolean isTimeDateOK = false;
        ObservableList<Ticket> selectedTicket = null;
        try {
            //get selected row
            selectedTicket = ticketTableView.getSelectionModel().getSelectedItems();
        } catch (NullPointerException e) {
            System.out.println("Please select a row " + e.getMessage());
        }
        //here we find number of sold ticket , capacity of airplane, date to flight and taken time
        for (int i = 0; i < Flight.flights.size(); i++) {
            if (Flight.flights.get(i).getID().equals(selectedTicket.get(0).getFlightID())) {

                //found number of sold ticket, date to flight, time to flight and taken time
                numberOfSoldTicket = Integer.parseInt(Flight.flights.get(i).getNumberOfSoldTicket());
                dateToFlight = LocalDate.parse(Flight.flights.get(i).getDateToFight());
                timeToFlight = LocalTime.parse(Flight.flights.get(i).getTimeToFight());

                for (int j = 0; j < Airplane.airplanes.size(); j++) {
                    if (Airplane.airplanes.get(j).getID().equals(Flight.flights.get(i).getAirplaneID())) {
                        //find capacity of airplane
                        capacity = Integer.parseInt(Airplane.airplanes.get(j).getNumberOfSeats());
                        break;
                    }

                }

                if (capacity != 0)
                    break;
            }
        }


        //here we find price of the ticket
        price = Integer.parseInt(selectedTicket.get(0).getPrice());


        //here we find credit of the passenger
        for (int i = 0; i < Passenger.passengers.size(); i++) {
            if (Passenger.passengers.get(i).getUserName().equals(saveUsername)) {
                //found it
                credit = Integer.parseInt(Passenger.passengers.get(i).getCredit());
                //save index of the passenger
                indexOfPassenger = i;
                break;
            }
        }
        //here we compare date and time of ticket to others
        //we found information of previous flights has been reserved
        if (Passenger.passengers.get(indexOfPassenger).getTickets().size() != 0) {
            for (int j = 0; j < Passenger.passengers.get(indexOfPassenger).getTickets().size(); j++) {

                Flight flight = findFlight(Passenger.passengers.get(indexOfPassenger).getTickets().get(j).getID());
                //check date
                LocalDate date = LocalDate.parse(flight.getDateToFight());
                if (dateToFlight.getYear() == date.getYear() && dateToFlight.getMonth().equals(date.getMonth()) && dateToFlight.getDayOfMonth() == date.getDayOfMonth()) {
                    //check time
                    LocalTime time = LocalTime.parse(flight.getTimeToFight());//time to flight
                    LocalTime tTime = LocalTime.parse(flight.getTakenTime());//taken time

                    int hour = 0, minute = 0, second = 0;
                    //save range of previous time ticket
                    hour = time.getHour() + tTime.getHour();
                    minute = time.getMinute() + tTime.getMinute();
                    second = time.getSecond() + tTime.getSecond();
                    //if sum of them is more than 60 minute or 60 second
                    if (second >= 60) {
                        //convert seconds to minutes
                        second = (time.getSecond() + tTime.getSecond()) % 60;
                        minute += (time.getSecond() + tTime.getSecond()) / 60;
                    }
                    if (minute >= 60) {
                        //convert minutes to hours
                        minute = (time.getMinute() + tTime.getMinute()) % 60;
                        hour += (time.getMinute() + tTime.getMinute()) / 60;

                    }
                    //if there is flight interference
                    if ((timeToFlight.getHour() >= time.getHour() && timeToFlight.getHour() <= hour) || (timeToFlight.getMinute() >= time.getMinute() && timeToFlight.getMinute() <= minute) || (timeToFlight.getSecond() >= time.getSecond() && timeToFlight.getSecond() <= second)) {
                        isTimeDateOK = false;
                    } else {
                        isTimeDateOK = true;
                    }
                    //if date of the ticket is different from previous one
                }
            }

        } else {
            isTimeDateOK = true;
        }


        //compare credit and price of ticket
        if (price <= credit) {
            enoughCredit = true;
        }

        //check is there capacity or not
        if (numberOfSoldTicket < capacity) {
            enoughCapacity = true;
        }

        isBought = Check.checkTicket(selectedTicket.get(0).getID(), Passenger.passengers.get(indexOfPassenger).getID());

        //these if...else block show appropriate error if there isn't error we allow to passenger to buy this ticket
        if (!isBought) {
            ErrorPageController.text = "You already bought this ticket";
            ErrorPageController.createErrorWindow();
        } else if (!isTimeDateOK) {
            ErrorPageController.text = "There is flight interference";
            ErrorPageController.createErrorWindow();
        } else if (!enoughCapacity) {
            ErrorPageController.text = "There is no capacity";
            ErrorPageController.createErrorWindow();
        } else if (!enoughCredit) {
            ErrorPageController.text = "There isn't enough credit";
            ErrorPageController.createErrorWindow();
        } else {
            //here we insert passengerID and ticketID to database and array list
            TicketsAndPassengers.ticketsAndPassengers.add(new TicketsAndPassengers(Passenger.passengers.get(indexOfPassenger).getID(), selectedTicket.get(0).getID()));
            Database.setInformation(TicketsAndPassengers.ticketsAndPassengers.get(TicketsAndPassengers.ticketsAndPassengers.size() - 1));
            //here we set new credit to the passenger
            int newCredit = credit - price;
            Passenger.passengers.get(indexOfPassenger).setCredit(Integer.toString(newCredit));


            //here we set new value to the number of sold ticket
            for (int i = 0; i < Flight.flights.size(); i++) {
                if (Flight.flights.get(i).getID().equals(selectedTicket.get(0).getFlightID())) {
                    //found it
                    numberOfSoldTicket++;
                    Flight.flights.get(i).setNumberOfSoldTicket(Integer.toString(numberOfSoldTicket));
                }
            }
            Database.updateInformationTicket(Integer.toString(numberOfSoldTicket), selectedTicket.get(0).getFlightID(), Integer.toString(newCredit), saveUsername);
            //update lists
            Main.setTicketsAndPassengers();
            Main.setPassengers();
            stage.close();
        }


    }

    //set on action for cancel button
    public void setOnActionCancelBTN(ActionEvent event) {
        //get current window
        Stage stage = (Stage) cancelBTN.getScene().getWindow();

        //this variable save ticket price of canceling
        int priceOfCanceling = 0;
        //this variable save flight id
        String flightID = "";
        //this variable check that selected ticket bought by passenger
        boolean isBought = false;

        ObservableList<Ticket> selectedTicket = null;
        try {
            //get selected row
            selectedTicket = ticketTableView.getSelectionModel().getSelectedItems();
            //here we set flight id of selection ticket
            flightID = selectedTicket.get(0).getFlightID();
        } catch (NullPointerException e) {
            System.out.println("Please select a row " + e.getMessage());
        }

        //here we check the passenger that bought this ticket or didn't
        for (int i = 0; i < Flight.flights.size(); i++) {
            if (Flight.flights.get(i).getID().equals(flightID)) {
                for (int j = 0; j < Flight.flights.get(i).getPassengers().size(); j++) {
                    if (Flight.flights.get(i).getPassengers().get(j).getUserName().equals(saveUsername)) {
                        isBought = true;
                        break;
                    }
                }
                if (isBought)
                    break;
            }
        }

        if (isBought) {
        //here we set new value for number of sold ticket
        for (int i = 0; i < Flight.flights.size(); i++) {
            if (Flight.flights.get(i).getID().equals(flightID)) {
                //found it
                int numberOfSoldTicket = Integer.parseInt(Flight.flights.get(i).getNumberOfSoldTicket());
                numberOfSoldTicket--;
                String newNumberOfSoldTicket = Integer.toString(numberOfSoldTicket);
                //set new value
                Flight.flights.get(i).setNumberOfSoldTicket(newNumberOfSoldTicket);
                break;
            }
        }

        //here we set ticket price of cancelling
        priceOfCanceling = Integer.parseInt(selectedTicket.get(0).getPriceOfCancelling());

        //here we set new credit and set null for flight id
        for (int i = 0; i < Passenger.passengers.size(); i++) {
            if (Passenger.passengers.get(i).getUserName().equals(saveUsername)) {
                int credit = Integer.parseInt(Passenger.passengers.get(i).getCredit());
                int newCredit = credit + priceOfCanceling;
                //set new credit
                Passenger.passengers.get(i).setCredit(Integer.toString(newCredit));
                //remove this ticket and passenger from database and array list
                Database.removeInformation(new TicketsAndPassengers(), selectedTicket.get(0).getID());
                Database.getInformation(new TicketsAndPassengers());
            }
        }
        //update lists
        Main.setPassengers();
        Main.setTicketsAndPassengers();


        //update passenger table in database
        for (int i = 0; i < Passenger.passengers.size(); i++) {
            Database.updateInformation(Passenger.passengers.get(i), Passenger.passengers.get(i).getID());
        }

        //update flight table in database
        for (int i = 0; i < Flight.flights.size(); i++) {
            Database.updateInformation(Flight.flights.get(i), Flight.flights.get(i).getID());
        }
        stage.close();

    }else {
            ErrorPageController.text = "You never bought this ticket";
            ErrorPageController.createErrorWindow();
        }

}

    //this method find flight object
    private Flight findFlight(String ticketID){
        String saveFlightID = "";
        Flight flight = new Flight();
        //here we find flightID that related to ticketID
        for (int i = 0; i < Ticket.tickets.size(); i++) {
            if(Ticket.tickets.get(i).getID().equals(ticketID)){
                saveFlightID = Ticket.tickets.get(i).getFlightID();
            }
        }
        //here we find Flight object with particular ID
        for (int i = 0; i < Flight.flights.size(); i++) {
            if(Flight.flights.get(i).getID().equals(saveFlightID)){
                flight = Flight.flights.get(i);
            }
        }
        return flight;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        id.setCellValueFactory(new PropertyValueFactory<>("iD"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        priceCancelling.setCellValueFactory(new PropertyValueFactory<>("priceOfCancelling"));
        flightID.setCellValueFactory(new PropertyValueFactory<>("flightID"));
        Platform.runLater(() -> {
            boolean printOnce = false;
            if(isEnterParticularFlightID){
                for (int i = 0; i < Flight.flights.size(); i++) {
                    //find the flight with particular airplane ID
                    if( Flight.flights.get(i).getAirplaneID().equals(saveAirplaneID)) {
                        for (Ticket t : Flight.flights.get(i).getTickets()) {
                            if(t.getFlightID().equals(saveFlightID)) {
                                //find the ticket with particular airplane ID and flight ID
                                ticketTableView.getItems().add(t);
                                printOnce = true;
                            }
                        }
                        if(printOnce)
                            break;

                    }
                }

            }
        });
    }
}
