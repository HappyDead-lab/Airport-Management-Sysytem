package controller.Ticket;

import Main.Main;
import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Flight;
import model.Ticket;
import model.TicketsAndPassengers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TicketInformationPageController implements Initializable {

    private @FXML Button exitBTN;
    private @FXML TableView<Ticket> ticketTableView;
    private @FXML TableColumn<Ticket,String> id;
    private @FXML TableColumn<Ticket,String> price;
    private @FXML TableColumn<Ticket,String> priceCancelling;
    private @FXML TableColumn<Ticket,String> flightID;
    private boolean isEnterParticularFlightID = false;
    private String saveAirplaneID;
    //make window movable
    private double xOffset = 0;
    private double yOffset = 0;

    public void setEnterParticularFlightID(boolean enterParticularFlightID) {
        this.isEnterParticularFlightID = enterParticularFlightID;
    }

    public void setSaveAirplaneID(String saveAirplaneID) {
        this.saveAirplaneID = saveAirplaneID;
    }

    public void setSaveFlightID(String saveFlightID) {
        this.saveFlightID = saveFlightID;
    }

    private String saveFlightID;
    //set on action for exit button
    public void setOnActionExitBTN(ActionEvent event){
        Stage stage = (Stage)exitBTN.getScene().getWindow();
        stage.close();
    }

    //set on action for remove button
    public void setOnActionRemoveBTN(ActionEvent event) {
        //get all information from table view
        ObservableList<Ticket> allTickets, selectionTicket = null;
        allTickets = ticketTableView.getItems();

        try {
            //get selection row from table view
            selectionTicket = ticketTableView.getSelectionModel().getSelectedItems();
        }catch (NullPointerException e){
            System.out.println("Please select a row " + e.getMessage());
        }
        for (int i = 0; i < selectionTicket.size(); i++) {
            //find selection object in array list to remove it from database too
            for (int j = 0; j < Ticket.tickets.size(); j++) {
                if (selectionTicket.get(i).getID().equals(Ticket.tickets.get(j).getID())) {
                    Database.removeInformation(new Ticket(), Ticket.tickets.get(j).getID());
                    Ticket.tickets.remove(j);
                    break;
                }
            }
        }
        //update related tables in database
        Database.getInformation(new TicketsAndPassengers());
        //update lists
        Main.setTicketsAndPassengers();
        Main.setTickets();
        Main.setPassengers();
        selectionTicket.forEach(allTickets::remove);
    }

    //set on action for add button
    public void setOnActionAddBTN(ActionEvent event){
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Ticket/AddTicketPage.fxml"));
        //create root
        AnchorPane root = null;
        try {
            //get root
            root = (AnchorPane) loader.load();
            //get controller
            AddTicketPageController controller = loader.getController();
            controller.setTicketTableView(ticketTableView);
            controller.setEnterParticularFlightID(isEnterParticularFlightID);
            controller.setSaveFlightID(saveFlightID);
        } catch (IOException e) {
            e.printStackTrace();
        }


        //get mouse position
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set mouse position
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });
        //set scene
        stage.setScene(new Scene(root));
        stage.show();
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        id.setCellValueFactory(new PropertyValueFactory<>("iD"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        priceCancelling.setCellValueFactory(new PropertyValueFactory<>("priceOfCancelling"));
        flightID.setCellValueFactory(new PropertyValueFactory<>("flightID"));

        //below line is for prevent of getting null variables
        Platform.runLater(() ->{
            boolean printOnce = false;
            if(isEnterParticularFlightID){
                for (int i = 0; i < Flight.flights.size(); i++) {
                    //find the flight with particular airplane ID
                    if( Flight.flights.get(i).getAirplaneID().equals(saveAirplaneID)) {
                        for (Ticket t : Flight.flights.get(i).getTickets()) {
                            if(t.getFlightID().equals(saveFlightID)) {
                                //find the ticket with particular airplane ID and flight ID
                                ticketTableView.getItems().add(t);
                                printOnce = true;
                            }
                        }
                        if(printOnce)
                            break;

                    }
                }

            }else {
                for (int i = 0; i < Ticket.tickets.size(); i++) {
                    ticketTableView.getItems().add(Ticket.tickets.get(i));
                }
            }
        });
        editTableColumn();
    }

    //this method make table view editable
    public void editTableColumn() {

        //make name field editable
        price.setCellFactory(TextFieldTableCell.forTableColumn());
        price.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPrice = false;
            isPrice = Check.checkNumber(event.getNewValue());
            if(isPrice) {
                //get selected row from table view
                ObservableList<Ticket> selectedItem;
                selectedItem = ticketTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getID();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPrice(event.getNewValue());
                //change value in array list and database
                updateTicket(uniqValue, event.getNewValue(), "price");

            }else {
                createErrorWindow("Please enter number ");
            }
        });


        //make last name field editable
        priceCancelling.setCellFactory(TextFieldTableCell.forTableColumn());
        priceCancelling.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPrice = false;
            isPrice = Check.checkNumber(event.getNewValue());
            if(isPrice) {
                //get selected row from table view
                ObservableList<Ticket> selectedItem;
                selectedItem = ticketTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getID();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPriceOfCancelling(event.getNewValue());
                //change value in array list and database
                updateTicket(uniqValue, event.getNewValue(), "priceCancelling");
            }else {
                createErrorWindow("Please enter number ");
            }
        });

        ticketTableView.setEditable(true);
    }

    //this method will be always make array list of Employee update
    private void updateTicket(String username, String newValue, String property) {

        //change old price with new one
        if (property.equals("price")) {
            for (int i = 0; i < Ticket.tickets.size(); i++) {
                if (Ticket.tickets.get(i).getID().equals(username)) {
                    Ticket.tickets.get(i).setPrice(newValue);
                }
            }
        }

        //change old price cancelling with new one
        if (property.equals("priceCancelling")) {
            for (int i = 0; i < Ticket.tickets.size(); i++) {
                if (Ticket.tickets.get(i).getID().equals(username)) {
                    Ticket.tickets.get(i).setPriceOfCancelling(newValue);
                }
            }
        }

        //update Flight table in database
        for (int i = 0; i < Ticket.tickets.size(); i++) {
            Database.updateInformation(Ticket.tickets.get(i),Ticket.tickets.get(i).getID());
        }

    }

    //this method create an error window with particular message
    private void createErrorWindow(String message) {
        ErrorPageController.text = message;
        ErrorPageController.createErrorWindow();
    }

}
