package controller.Ticket;

import Main.Main;
import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import model.Ticket;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AddTicketPageController implements Initializable {
    private @FXML Button cancelBTN;
    private @FXML Button addBTN;
    private @FXML TextField flightIDTXF;
    private @FXML TextField priceTXF;
    private @FXML TextField priceOfCancellingTXF;
    private @FXML Label invalidFlightIDLBL;
    private @FXML Label invalidPriceLBL;
    private @FXML Label invalidPriceOfCancellingLBL;

    public void setTicketTableView(TableView<Ticket> ticketTableView) {
        this.ticketTableView = ticketTableView;
    }

    private TableView<Ticket> ticketTableView ;
    private boolean isEnterParticularFlightID;
    private String saveFlightID;
    private Ticket ticket = new Ticket();

    public void setEnterParticularFlightID(boolean enterParticularFlightID) {
        isEnterParticularFlightID = enterParticularFlightID;
    }

    public void setSaveFlightID(String saveFlightID) {
        this.saveFlightID = saveFlightID;
    }

    //set on action for cancel button
    public void setOnActionCancelBTN(ActionEvent event){
        Stage stage = (Stage)cancelBTN.getScene().getWindow();
        stage.close();
    }
    //set on action for add button
    public void setOnActionAddBTN(ActionEvent event) {
        Stage stage = (Stage)addBTN.getScene().getWindow();
        if (flightIDTXF.getText().equals("") || priceTXF.getText().equals("") || priceOfCancellingTXF.getText().equals("")) {
            //show error if one of the fields be empty
            ErrorPageController.text = "Please fill all parts of the fields";

            AnchorPane root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/view/ErrorPage.fxml"));
            } catch (IOException e) {
                System.out.println("we cannot load error page " + " " + e.getMessage());
                e.printStackTrace();
            }

            stage = new Stage(StageStyle.UNDECORATED);
            stage.initStyle(StageStyle.TRANSPARENT);
            Scene scene = new Scene(root);
            scene.setFill(Color.TRANSPARENT);
            stage.setScene(scene);
            stage.show();
        }else{
            //to check that some input value is correct or not
            boolean isFlightID = false;
            boolean isPrice = false;
            boolean isPriceOfCancelling = false;
            //if was not entered by show button
            if(!isEnterParticularFlightID){
                isFlightID = Check.checkFlightID(flightIDTXF.getText());
                if(isFlightID){
                    ticket.setFlightID(flightIDTXF.getText());
                    invisible(invalidFlightIDLBL);
                }else {
                    //visible error label
                    visible(invalidFlightIDLBL);
                }
            }else {
                isFlightID = true;
            }
            //if price input is valid
            isPrice = Check.checkNumber(priceTXF.getText());
            if(isPrice){
                ticket.setPrice(priceTXF.getText());
                invisible(invalidPriceLBL);
            }else {
                //visible error label
                visible(invalidPriceLBL);
            }
            //if price of cancelling input is valid
            isPriceOfCancelling = Check.checkNumber(priceOfCancellingTXF.getText());
            if(isPriceOfCancelling){
                ticket.setPriceOfCancelling(priceOfCancellingTXF.getText());
                invisible(invalidPriceOfCancellingLBL);
            }else {
                //visible error label
                visible(invalidPriceOfCancellingLBL);
            }

            if(isFlightID && isPrice && isPriceOfCancelling){
                //invisible all label
                invisible(invalidFlightIDLBL);
                invisible(invalidPriceLBL);
                invisible(invalidPriceOfCancellingLBL);

                //save information on database
                Database.setInformation(ticket);
                //insert all tickets to it's array list
                Database.getInformation(new Ticket());
                //set id that get from database
                ticket.setID(Ticket.tickets.get(Ticket.tickets.size() - 1).getID());
                //update list of tickets for Flight class
                Main.setTickets();
                //add this object to table view
                ticketTableView.getItems().add(ticket);
                stage.close();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Platform.runLater(() -> {
            if(isEnterParticularFlightID){
                //set flight id and disable flight id text field
                flightIDTXF.setText(saveFlightID);
                flightIDTXF.setDisable(true);
                ticket.setFlightID(saveFlightID);

            }
        });
    }


    //make a label visible
    private void visible(Label label){
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //make a label invisible
    private void invisible(Label label){
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }
}
