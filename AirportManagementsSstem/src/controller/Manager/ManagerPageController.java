package controller.Manager;

import Main.Main;
import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import model.Manager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ManagerPageController implements Initializable {
    private @FXML
    Button editProfileBTN;
    private @FXML
    Button changePassBTN;
    private @FXML
    Button changeBTN;
    private @FXML
    Button informationFlightBTN;
    private @FXML
    Button informationAirplaneBTN;
    private @FXML
    PasswordField passwordPSF;
    private @FXML
    PasswordField confirmationPSF;
    private  @FXML
    Label successfulLBL;

    //for make pages movable
    private double xOffset = 0;
    private double yOffset = 0;

    //this boolean is for personal profile management button
    private boolean checkPersonalBTN = false;

    //this boolean is for change password button
    private boolean checkChangeBTN = false;

    //this boolean is for airplane and flight management button
    private boolean checkAirplaneAndFlightManagementBTN = false;

    //this variable for save input username that manager entered with it
    private static String saveUsername;

    public static void setUsername(String username) {
        saveUsername = username;
    }

    public static String getUsername() {
        return saveUsername;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    //set on action for airplane and flight management button
    public void setOnActionAirplaneAndFlightManagementBTN(ActionEvent event) {
        if (!checkAirplaneAndFlightManagementBTN) {
            //first click
            visibleBTN(informationFlightBTN, 0.5);
            visibleBTN(informationAirplaneBTN, 0.75);
            checkAirplaneAndFlightManagementBTN = true;
        } else if (checkAirplaneAndFlightManagementBTN) {
            //second click
            invisibleBTN(informationAirplaneBTN, 0.5);
            invisibleBTN(informationFlightBTN, 0.75);
            checkAirplaneAndFlightManagementBTN = false;
        }
    }

    //set on action for information flight button
    public void setOnActionInformationFlightBTN(ActionEvent event) {
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);
        //load page
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Flight/FlightInformationPage.fxml"));

        BorderPane root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //get mouse position
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });


        //set mouse position
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });
        //set scene
        stage.setScene(new Scene(root));
        stage.show();
    }

    //set on action for employee management button
    public void setOnActionEmployeeManagementBTN(ActionEvent event) {
        //create stage
        Stage stage = new Stage(StageStyle.DECORATED);
        BorderPane root = new BorderPane();
        try {
            //load page
            root = FXMLLoader.load(getClass().getResource("/view/Employee/EmployeeInformationPage.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //get mouse position
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });


        //set mouse position
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });

        //set scene
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    //set on action for personal profile management button
    public void setOnActionPersonalBTN(ActionEvent event) {
        if (!checkPersonalBTN) {
            //for first click
            visibleBTN(editProfileBTN, 0.50);
            visibleBTN(changePassBTN, 0.75);
            checkPersonalBTN = true;
        } else if (checkPersonalBTN) {
            //for second click
            invisibleLBL(successfulLBL, 0.50);
            invisibleBTN(changePassBTN, 1.50);
            invisibleBTN(changeBTN, 0.95);
            invisiblePSF(confirmationPSF, 1.25);
            invisiblePSF(passwordPSF, 1.40);
            invisibleBTN(editProfileBTN, 1.60);
            checkPersonalBTN = false;
        }

    }


    //set on action for change password button
    public void setOnActionChangePassBTN(ActionEvent event) {
        if (!checkChangeBTN) {
            //for first click
            visiblePSF(passwordPSF, 0.75);
            visiblePSF(confirmationPSF, 0.95);
            visibleBTN(changeBTN, 1.25);
            checkChangeBTN = true;
        } else if (checkChangeBTN) {
            //for second click
            invisibleLBL(successfulLBL, 0.50);
            invisibleBTN(changeBTN, 0.95);
            invisiblePSF(confirmationPSF, 1.25);
            invisiblePSF(passwordPSF, 1.40);
            checkChangeBTN = false;
        }
    }


    //set on action for change button
    public void setOnActionChangeBTN(ActionEvent event) {


        //show appropriate error
        if (passwordPSF.getText().equals("") || confirmationPSF.getText().equals("")) {
            //if at least one of field is empty

            //set message of error
            ErrorPageController.text = "Please fill all of the fields";
            ErrorPageController.createErrorWindow();

        } else if (!(passwordPSF.getText().equals(confirmationPSF.getText()))) {
            //if new password is not equal to its confirmation

            //set message of error
            ErrorPageController.text = "Please enter correct confirmation";
            ErrorPageController.createErrorWindow();
        } else {
            //change password of manager
            boolean isPass = Check.checkPassword(passwordPSF.getText());
            if (isPass) {

                //here we find the user that enter an change password
                for (int i = 0; i < Manager.managers.size(); i++) {
                    if (Manager.managers.get(i).getUserName().equals(saveUsername)) {
                        Manager.managers.get(i).setPassword(passwordPSF.getText());
                        Database.changePassword(new Manager(), saveUsername, passwordPSF.getText());
                        break;
                    }
                }

                //clear password fields
                passwordPSF.clear();
                confirmationPSF.clear();


                visibleLBL(successfulLBL, 0.1);

            } else {
                ErrorPageController.text = "It is less than 5 character";
                ErrorPageController.createErrorWindow();
            }
        }
    }

    //set on action for message management button
    public void setOnActionMessageManagementBTN(ActionEvent event) {
        //create stage and set style
        Stage stage = new Stage(StageStyle.UNDECORATED);
        //create root
        BorderPane root = null;
        //load page
        try {
            root = FXMLLoader.load(getClass().getResource("/view/ShowMessagePage.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //get mouse position
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set mouse position
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });


        //set scene
        stage.setScene(new Scene(root));
        stage.show();
    }

    //set on action for edit profile button
    public void setOnActionEditProfileBTN(ActionEvent event) {
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/view/Manager/EditManagerProfile.fxml"));
        //create root
        BorderPane root = null;
        try {
            //load page
            root = (BorderPane) loader.load();
            EditManagerProfileController edit = loader.getController();
            edit.setUser(saveUsername);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //get mouse position
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set mouse position
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });

        stage.setScene(new Scene(root));
        stage.show();
    }

    //set on action for passengers management button
    public void setOnActionPassengersManagementBTN(ActionEvent event) {
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);
        //create root
        BorderPane root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/Passenger/PassengerInformationPage.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //get mouse position
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set mouse position
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });

        stage.setScene(new Scene(root));
        stage.show();
    }

    //set on action for information airplane button
    public void setOnActionInformationAirplaneBTN(ActionEvent event) {
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);

        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/view/Airplane/AirplaneInformationPage.fxml"));
        //create root
        BorderPane root = null;
        try {
            root = (BorderPane) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //set style
        stage.initStyle(StageStyle.TRANSPARENT);
        //create scene
        Scene scene = new Scene(root);
        //set style
        scene.setFill(Color.TRANSPARENT);
        //get mouse position
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set mouse position
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });
        //set scene
        stage.setScene(scene);
        stage.show();


    }


    //this method visible a button
    private void visibleBTN(Button button, double time) {

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                button.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //this method invisible a button
    private void invisibleBTN(Button button, double time) {

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it invisible after a few second
                                button.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

    //this method visible a label
    private void visibleLBL(Label label, double time) {

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //this method visible a label
    private void invisibleLBL(Label label, double time) {

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it invisible after a few second
                                label.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

    //this method visible a password field
    private void visiblePSF(PasswordField passwordField, double time) {

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                passwordField.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

    //this method invisible a password field
    private void invisiblePSF(PasswordField passwordField, double time) {

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it invisible after a few second
                                passwordField.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


}
