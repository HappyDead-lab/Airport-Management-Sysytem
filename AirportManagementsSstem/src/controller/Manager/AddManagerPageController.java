package controller.Manager;

import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Manager;

public class AddManagerPageController {
    @FXML
    private TextField firstNameTXF;
    @FXML
    private TextField lastNameTXF;
    @FXML
    private TextField phoneNumberTXF;
    @FXML
    private TextField addressTXF;
    @FXML
    private TextField usernameTXF;
    @FXML
    private TextField emailTXF;
    @FXML
    private TextField salaryTXF;
    @FXML
    private PasswordField passwordPSF;
    @FXML
    private PasswordField confirmationPSF;
    @FXML
    private Button addBTN;
    @FXML
    private Button cancelBTN;
    @FXML
    private Label invalidPhoneNumberLBL;
    @FXML
    private Label invalidEmailLBL;
    @FXML
    private Label invalidUsername;
    @FXML
    private Label invalidSalaryLBL;
    @FXML
    private Label invalidPassLBL;
    private TableView<Manager> managerTableView;

    public void setTableView(TableView<Manager> tableView) {
        this.managerTableView = tableView;
    }


    //set on action for cancel button
    public void setOnActionCancelBTN(ActionEvent event) {
        //get current stage
        Stage stage = (Stage) cancelBTN.getScene().getWindow();

        stage.close();
    }


    //set on action for add button
    public void setOnActionAddBTN(ActionEvent event) {
        //get current stage
        Stage stage = (Stage) addBTN.getScene().getWindow();

        if (firstNameTXF.getText().equals("") || lastNameTXF.getText().equals("") || phoneNumberTXF.getText().equals("") || addressTXF.getText().equals("") || usernameTXF.getText().equals("") || emailTXF.getText().equals("") || salaryTXF.getText().equals("") || passwordPSF.getText().equals("") || confirmationPSF.getText().equals("")) {
            //show error if one of the fields be empty
            //set error message
            ErrorPageController.text = "Please fill all parts of the fields";
            ErrorPageController.createErrorWindow();
        } else if (!(passwordPSF.getText().equals(confirmationPSF.getText()))) {
            //if password does not equal to confirmation of password
            //set error message
            ErrorPageController.text = "Please enter correct confirmation";
            ErrorPageController.createErrorWindow();
        } else {
            //if there are not any errors
            //to check that some input value is correct or not
            boolean isPhoneNumber = false;
            boolean isUsername = false;
            boolean isEmail = false;
            boolean isSalary = false;
            boolean isPassword = false;

            Manager manager = new Manager();

            //check input salary is valid or not
            isSalary = Check.checkNumber(salaryTXF.getText());
            if (isSalary) {
                //valid
                invisible(invalidSalaryLBL);
                manager.setSalary(salaryTXF.getText());
            } else {
                //invalid
                //visible error label
                visible(invalidSalaryLBL);
            }

            //check input email is valid or not
            isEmail = Check.checkEmailManager(emailTXF.getText());
            if (isEmail) {
                //valid
                invisible(invalidEmailLBL);
                manager.setEmail(emailTXF.getText());
            } else {
                //invalid
                //visible error label
                visible(invalidEmailLBL);
            }

            //check input username is valid or not
            isUsername = Check.checkUsernameManager(usernameTXF.getText());
            if (isUsername) {
                //valid
                invisible(invalidUsername);
                manager.setUserName(usernameTXF.getText());
            } else {
                //invalid
                //visible error label
                visible(invalidUsername);
            }


            //check input phone number is valid or not
            isPhoneNumber = Check.checkPhoneNumber(phoneNumberTXF.getText());
            if (isPhoneNumber) {
                //valid
                invisible(invalidPhoneNumberLBL);
                manager.setPhoneNumber(phoneNumberTXF.getText());
            } else {
                //invalid
                //visible error label
                visible(invalidPhoneNumberLBL);
            }

            isPassword = Check.checkPassword(passwordPSF.getText());
            if (isPassword) {
                //valid
                invisible(invalidPassLBL);
                manager.setPassword(passwordPSF.getText());
            } else {
                //invalid
                //visible error label
                visible(invalidPassLBL);
            }

            if (isEmail && isPhoneNumber && isUsername && isSalary && isPassword) {
                //invisible all labels
                invisible(invalidPassLBL);
                invisible(invalidPhoneNumberLBL);
                invisible(invalidUsername);
                invisible(invalidEmailLBL);
                invisible(invalidSalaryLBL);

                //add remains information
                manager.setName(firstNameTXF.getText());
                manager.setLastName(lastNameTXF.getText());
                manager.setPassword(passwordPSF.getText());
                manager.setAddress(addressTXF.getText());
                manager.setSalary(salaryTXF.getText());

                //save information on database
                Database.setInformation(manager);
                //save information in array list from database
                Database.getInformation(manager);
                manager.setID(Manager.managers.get(Manager.managers.size() - 1).getID());
                //add information to table view at the moment
                managerTableView.getItems().add(manager);
                stage.close();
            }
        }
    }

    //make a label visible
    private void visible(Label label) {
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //make a label invisible
    private void invisible(Label label) {
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


}
