package controller.Manager;

import Main.Main;
import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Manager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ManagerInformationPageController implements Initializable {
    private @FXML
    Button exitBTN;
    private @FXML
    TableView<Manager> managerTableView;
    private @FXML
    TableColumn<Manager, String> id;
    private @FXML
    TableColumn<Manager, String> name;
    private @FXML
    TableColumn<Manager, String> lastName;
    private @FXML
    TableColumn<Manager, String> username;
    private @FXML
    TableColumn<Manager, String> password;
    private @FXML
    TableColumn<Manager, String> email;
    private @FXML
    TableColumn<Manager, String> address;
    private @FXML
    TableColumn<Manager, String> phoneNumber;
    private @FXML
    TableColumn<Manager, String> salary;

    //this variables help that make window movable
    private double xOffset = 0;
    private double yOffset = 0;


    //set on action for remove button
    public void setOnActionRemoveBTN(ActionEvent event) {
        ObservableList<Manager> allManagers, selectionManager = null;
        //get all information in table view
        allManagers = managerTableView.getItems();
        //maybe this value produce null pointer
        try {
            //get selection row
            selectionManager = managerTableView.getSelectionModel().getSelectedItems();
        } catch (NullPointerException e) {
            System.out.println("we are in manager information page controller in remove method " + e.getMessage());
        }

        //find selection user in array list
        for (int i = 0; i < selectionManager.size(); i++) {
            for (int j = 0; j < Manager.managers.size(); j++) {
                if (selectionManager.get(i).getUserName().equals(Manager.managers.get(j).getUserName())) {
                    //found it
                    //remove it from database
                    Database.removeInformation(new Manager(), Manager.managers.get(j).getUserName());
                    //remove it from array list
                    Manager.managers.remove(j);
                    break;
                }
            }
        }
        //remove it from table view
        selectionManager.forEach(allManagers::remove);
    }

    //set on action for exit button
    public void setOnActionExitBTN(ActionEvent event) {
        //get current stage
        Stage stage = (Stage) exitBTN.getScene().getWindow();
        stage.close();
    }


    //set on action for add button
    public void setOnActionAddBTN(ActionEvent event) {
        //create stage and set style
        Stage stage = new Stage(StageStyle.UNDECORATED);

        //load manage information page
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/view/Manager/AddManagerPage.fxml"));
        try {
            //load page
            loader.load();
            //get controller
            AddManagerPageController controller = loader.getController();
            controller.setTableView(managerTableView);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //get root
        AnchorPane root = loader.getRoot();

        //get position of mouse
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        //set position of mouse
        root.setOnMouseDragged(e -> {
            stage.setX(e.getScreenX() - xOffset);
            stage.setY(e.getScreenY() - yOffset);
        });

        //set scene
        stage.setScene(new Scene(root));

        stage.show();

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if (Manager.managers.size() != 0) {
            id.setCellValueFactory(new PropertyValueFactory<>("iD"));
            name.setCellValueFactory(new PropertyValueFactory<>("name"));
            lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
            username.setCellValueFactory(new PropertyValueFactory<>("userName"));
            password.setCellValueFactory(new PropertyValueFactory<>("password"));
            email.setCellValueFactory(new PropertyValueFactory<>("email"));
            address.setCellValueFactory(new PropertyValueFactory<>("address"));
            phoneNumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
            salary.setCellValueFactory(new PropertyValueFactory<>("salary"));
            //add information of managers in table view
            for (int i = 0; i < Manager.managers.size(); i++) {
                managerTableView.getItems().add(Manager.managers.get(i));
            }
        }
        editTableColumn();
    }

    //this method make table view editable
    private void editTableColumn() {

        //make name field editable
        name.setCellFactory(TextFieldTableCell.forTableColumn());
        name.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Manager> selectedItem;
            selectedItem = managerTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update value in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setName(event.getNewValue());
            //change value in array list and database
            updateManagers(uniqValue, event.getNewValue(), "name");
        });


        //make last name field editable
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Manager> selectedItem;
            selectedItem = managerTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update value in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setLastName(event.getNewValue());
            //change value in array list and database
            updateManagers(uniqValue, event.getNewValue(), "lastName");
        });

        //make username field editable
        username.setCellFactory(TextFieldTableCell.forTableColumn());
        username.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isUsername = false;
            isUsername = Check.checkUsernameManager(event.getNewValue());

            if (isUsername) {

                //get selected row from table view
                ObservableList<Manager> selectedItem;
                selectedItem = managerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setUserName(event.getNewValue());
                //change value in array list and database
                updateManagers(uniqValue, event.getNewValue(), "username");

            } else {
                createErrorWindow("This username already exists ");
            }
        });

        //make password field editable
        password.setCellFactory(TextFieldTableCell.forTableColumn());
        password.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPassword = false;
            isPassword = Check.checkPassword(event.getNewValue());

            if (isPassword) {

                //get selected row from table view
                ObservableList<Manager> selectedItem;
                selectedItem = managerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPassword(event.getNewValue());
                //change value in array list and database
                updateManagers(uniqValue, event.getNewValue(), "password");

            } else {
                createErrorWindow("It is less than 5 character");
            }
        });

        //make phone number field editable
        phoneNumber.setCellFactory(TextFieldTableCell.forTableColumn());
        phoneNumber.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPhoneNumber = false;
            isPhoneNumber = Check.checkPhoneNumber(event.getNewValue());

            if (isPhoneNumber) {

                //get selected row from table view
                ObservableList<Manager> selectedItem;
                selectedItem = managerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPhoneNumber(event.getNewValue());
                //change value in array list and database
                updateManagers(uniqValue, event.getNewValue(), "phoneNumber");

            } else {
                createErrorWindow("Invalid phone number");
            }
        });

        //make address field editable
        address.setCellFactory(TextFieldTableCell.forTableColumn());
        address.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Manager> selectedItem;
            selectedItem = managerTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setAddress(event.getNewValue());
            //change value in array list and database
            updateManagers(uniqValue, event.getNewValue(), "address");
        });

        //make email field editable
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isEmail = false;
            isEmail = Check.checkEmailManager(event.getNewValue());
            if (isEmail) {

                //get selected row from table view
                ObservableList<Manager> selectedItem;
                selectedItem = managerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setEmail(event.getNewValue());
                //change value in array list and database
                updateManagers(uniqValue, event.getNewValue(), "email");

            } else {
                createErrorWindow("Invalid email");
            }
        });

        //make salary field editable
        salary.setCellFactory(TextFieldTableCell.forTableColumn());
        salary.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isSalary = false;
            isSalary = Check.checkNumber(event.getNewValue());

            if (isSalary) {

                //get selected row from table view
                ObservableList<Manager> selectedItem;
                selectedItem = managerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setSalary(event.getNewValue());
                //change value in array list and database
                updateManagers(uniqValue, event.getNewValue(), "salary");

            } else {
                createErrorWindow("Please enter number");
            }
        });

        //make table view editable
        managerTableView.setEditable(true);
    }

    //this method always keeps update Manager table view and it's array list
    private void updateManagers(String username, String newValue, String property) {

        //change old name with new name
        if (property.equals("name")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(username)) {
                    Manager.managers.get(i).setName(newValue);
                    break;
                }
            }
        }

        //change old last name with new last name
        if (property.equals("lastName")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(username)) {
                    Manager.managers.get(i).setLastName(newValue);
                    break;
                }
            }
        }

        //change old username with new username
        if (property.equals("username")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(username)) {
                    Manager.managers.get(i).setUserName(newValue);
                    break;
                }
            }
        }

        //change old password with new password
        if (property.equals("password")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(username)) {
                    Manager.managers.get(i).setPassword(newValue);
                    break;
                }
            }
        }

        //change old phone number with new phone number
        if (property.equals("phoneNumber")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(username)) {
                    Manager.managers.get(i).setPhoneNumber(newValue);
                    break;
                }
            }
        }

        //change old address with new address
        if (property.equals("address")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(username)) {
                    Manager.managers.get(i).setAddress(newValue);
                    break;
                }
            }
        }

        //change old email with new email
        if (property.equals("email")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(username)) {
                    Manager.managers.get(i).setEmail(newValue);
                    break;
                }
            }
        }

        //change old salary with new salary
        if (property.equals("salary")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(username)) {
                    Manager.managers.get(i).setSalary(newValue);
                    break;
                }
            }
        }

        //update Manager table in database
        for (int i = 0; i < Manager.managers.size(); i++) {
            Database.updateInformation(Manager.managers.get(i), Manager.managers.get(i).getID());
        }
    }

    //this method shows error with given message
    private void createErrorWindow(String message) {
        ErrorPageController.text = message;
        ErrorPageController.createErrorWindow();
    }

}
