package controller.Manager;

import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import model.Manager;

import java.net.URL;
import java.util.ResourceBundle;

public class EditManagerProfileController implements Initializable {
    private @FXML
    TableView<Manager> managerTableView;
    private @FXML
    TableColumn<Manager, String> id;
    private @FXML
    TableColumn<Manager, String> name;
    private @FXML
    TableColumn<Manager, String> lastName;
    private @FXML
    TableColumn<Manager, String> username;
    private @FXML
    TableColumn<Manager, String> password;
    private @FXML
    TableColumn<Manager, String> email;
    private @FXML
    TableColumn<Manager, String> address;
    private @FXML
    TableColumn<Manager, String> phoneNumber;
    private @FXML
    TableColumn<Manager, String> salary;
    //save user username
    private String saveUsername;

    public void setUser(String username) {
        this.saveUsername = username;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (Manager.managers.size() != 0) {
            id.setCellValueFactory(new PropertyValueFactory<>("iD"));
            name.setCellValueFactory(new PropertyValueFactory<>("name"));
            lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
            username.setCellValueFactory(new PropertyValueFactory<>("userName"));
            password.setCellValueFactory(new PropertyValueFactory<>("password"));
            email.setCellValueFactory(new PropertyValueFactory<>("email"));
            address.setCellValueFactory(new PropertyValueFactory<>("address"));
            phoneNumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
            salary.setCellValueFactory(new PropertyValueFactory<>("salary"));
            //add information of user was entered
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(ManagerPageController.getUsername())) {
                    managerTableView.getItems().add(Manager.managers.get(i));
                    break;
                }
            }
        }

        editTableColumn();
    }

    //this method make table view editable
    private void editTableColumn() {

        //make name field editable
        name.setCellFactory(TextFieldTableCell.forTableColumn());
        name.setOnEditCommit(event -> {
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setName(event.getNewValue());
            //change value in array list and database
            updateManagers(event.getNewValue(), "name");
        });


        //make last name field editable
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> {
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setLastName(event.getNewValue());
            //change value in array list and database
            updateManagers(event.getNewValue(), "lastName");

        });

        //make username field editable
        username.setCellFactory(TextFieldTableCell.forTableColumn());
        username.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isUsername = false;
            isUsername = Check.checkUsernameManager(event.getNewValue());

            if (isUsername) {

                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setUserName(event.getNewValue());


                //change value in array list and database
                updateManagers(event.getNewValue(), "username");


            } else {
                createErrorWindow("This username already exists ");
            }
        });

        //make phone number field editable
        phoneNumber.setCellFactory(TextFieldTableCell.forTableColumn());
        phoneNumber.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPhoneNumber = false;
            isPhoneNumber = Check.checkPhoneNumber(event.getNewValue());

            if (isPhoneNumber) {
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPhoneNumber(event.getNewValue());


                //change value in array list and database
                updateManagers(event.getNewValue(), "phoneNumber");


            } else {
                createErrorWindow("Invalid phone number");
            }
        });

        //make address field editable
        address.setCellFactory(TextFieldTableCell.forTableColumn());
        address.setOnEditCommit(event -> {

            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setAddress(event.getNewValue());
            //change value in array list and database
            updateManagers(event.getNewValue(), "address");

        });

        //make email field editable
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isEmail = false;
            isEmail = Check.checkEmailManager(event.getNewValue());
            if (isEmail) {

                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setEmail(event.getNewValue());
                //change value in array list and database
                updateManagers(event.getNewValue(), "email");

            } else {
                createErrorWindow("Invalid email");
            }
        });

        //make table view editable
        managerTableView.setEditable(true);
    }

    //this method always keeps update Manager table view and it's array list
    private void updateManagers(String newValue, String property) {

        //change old name with new name
        if (property.equals("name")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(ManagerPageController.getUsername())) {
                    Manager.managers.get(i).setName(newValue);
                    break;
                }
            }
        }

        //change old last name with new last name
        if (property.equals("lastName")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(ManagerPageController.getUsername())) {
                    Manager.managers.get(i).setLastName(newValue);
                    break;
                }
            }
        }

        //change old username with new username
        if (property.equals("username")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(ManagerPageController.getUsername())) {
                    Manager.managers.get(i).setUserName(newValue);
                    saveUsername = newValue;
                    break;
                }
            }
        }
        //change old phone number with new phone number
        if (property.equals("phoneNumber")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(ManagerPageController.getUsername())) {
                    Manager.managers.get(i).setPhoneNumber(newValue);
                    break;
                }
            }
        }

        //change old address with new address
        if (property.equals("address")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(ManagerPageController.getUsername())) {
                    Manager.managers.get(i).setAddress(newValue);
                    break;
                }
            }
        }

        //change old email with new email
        if (property.equals("email")) {
            for (int i = 0; i < Manager.managers.size(); i++) {
                if (Manager.managers.get(i).getUserName().equals(ManagerPageController.getUsername())) {
                    Manager.managers.get(i).setEmail(newValue);
                    break;
                }
            }
        }

        //update Manager table in database
        for (int i = 0; i < Manager.managers.size(); i++) {
            Database.updateInformation(Manager.managers.get(i), Manager.managers.get(i).getID());
        }

    }

    //this method shows error with given message
    private void createErrorWindow(String message) {
        ErrorPageController.text = message;
        ErrorPageController.createErrorWindow();
    }
}
