package controller.Flight;

import Main.Main;
import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Flight;
import java.net.URL;
import java.util.ResourceBundle;

public class AddFlightPageController implements Initializable {
    private @FXML Button cancelBTN;
    private @FXML Button addBTN;
    private @FXML TextField originTXF;
    private @FXML TextField destinyTXF;
    private @FXML TextField airplaneIDTXF;
    private @FXML TextField timeToFlightTXF;
    private @FXML TextField takenTimeTXF;
    private @FXML DatePicker dataToFlightDPR;
    private @FXML Label invalidAirplaneIDLBL;
    private @FXML Label invalidTimeToFlightLBL;
    private @FXML Label invalidTakenTimeLBL;
    private @FXML Label invalidDateLBL;
    private TableView<Flight> tableView;
    private boolean isEnterParticularAirplaneID = false;

    public void setIsEnterParticularAirplaneID(boolean isEnterParticularAirplaneID) {
        this.isEnterParticularAirplaneID = isEnterParticularAirplaneID;
    }

    public void setSaveAirplaneID(String saveAirplaneID) {
        this.saveAirplaneID = saveAirplaneID;
    }

    private String saveAirplaneID ;

    public void setTableView(TableView<Flight> tableView) {
        this.tableView = tableView;
    }

    //set on action for cancel button
    public void setOnActionCancelBTN(ActionEvent event){
        Stage stage = (Stage)cancelBTN.getScene().getWindow();
        stage.close();
    }

    //set on action for add button
    public void setOnActionAddBTN(ActionEvent event){
        //get current stage
        Stage stage = (Stage)addBTN.getScene().getWindow();
        if(originTXF.getText().equals("") || destinyTXF.getText().equals("") || airplaneIDTXF.getText().equals("") || timeToFlightTXF.getText().equals("") || takenTimeTXF.getText().equals("") || String.valueOf(dataToFlightDPR.getValue()).equals("")){
            //show error if one of the fields be empty
            ErrorPageController.text = "Please fill all parts of the fields";
            ErrorPageController.createErrorWindow();
        }else{
            //to check that some input value is correct or not
            boolean isAirplaneID = false;
            boolean isTimeToFight = false;
            boolean isTakenTime = false;
            boolean isDate = false;

            Flight flight = new Flight();

                isDate = Check.checkDate(String.valueOf(dataToFlightDPR.getValue()));
                if(isDate){
                    //convert date to String
                    flight.setDateToFight(String.valueOf(dataToFlightDPR.getValue()));
                    invisible(invalidDateLBL);

                }else {
                    visible(invalidDateLBL);
                }



            //if we was entered by show button then set entered airplaneID as a airplaneId to this flight
            if(isEnterParticularAirplaneID){
                flight.setAirplaneID(saveAirplaneID);
                isAirplaneID = true;
            }else {
                isAirplaneID = Check.checkAirplaneID(airplaneIDTXF.getText());
                if (isAirplaneID) {
                    flight.setAirplaneID(airplaneIDTXF.getText());
                    invisible(invalidAirplaneIDLBL);
                } else {
                    visible(invalidAirplaneIDLBL);
                }
            }
            //check input format time
            isTakenTime = Check.checkTime(takenTimeTXF.getText());
            if(isTakenTime){
                flight.setTakenTime(takenTimeTXF.getText());
                invisible(invalidTakenTimeLBL);
            }else{
                visible(invalidTakenTimeLBL);
            }

            //check input format time
            isTimeToFight = Check.checkTime(timeToFlightTXF.getText());
            if(isTimeToFight){
                flight.setTimeToFight(timeToFlightTXF.getText());
                invisible(invalidTimeToFlightLBL);
            }else {
                visible(invalidTimeToFlightLBL);
            }

            if(isAirplaneID && isTakenTime && isTimeToFight && isDate){
                flight.setOrigin(originTXF.getText());
                flight.setDestiny(destinyTXF.getText());
                flight.setNumberOfSoldTicket("0");


                //save information on database
                Database.setInformation(flight);
                //save information in array list from database
                Database.getInformation(new Flight());
                flight.setID(Flight.flights.get(Flight.flights.size() - 1).getID());
                //update list of flights for Airplane class
                Main.setFlights();
                //add information to table view at the moment
                tableView.getItems().add(flight);
                stage.close();


            }

        }
    }

    //make a label visible
    private void visible(Label label){
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //make a label invisible
    private void invisible(Label label){
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //below line is for prevent of getting null variables
        Platform.runLater(() -> {
            if(isEnterParticularAirplaneID){
                airplaneIDTXF.setDisable(true);
                airplaneIDTXF.setText(saveAirplaneID);
            }
        });
    }
}
