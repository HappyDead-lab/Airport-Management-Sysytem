package controller.Flight;

import Main.Main;
import controller.*;
import controller.Passenger.PassengerInformationPageController;
import controller.Ticket.BuyTicketPageController;
import controller.Ticket.TicketInformationPageController;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class FlightInformationPageController implements Initializable {
    private @FXML Button addBTN;
    private @FXML Button removeBTN;
    private @FXML
    Button relatedPassengersBTN;
    private @FXML
    Button relatedTicketsBTN;
    private @FXML
    Button exitBTN;
    private @FXML
    TableView<Flight> flightTableView;
    private @FXML
    TableColumn<Flight, String> id;
    private @FXML
    TableColumn<Flight, String> origin;
    private @FXML
    TableColumn<Flight, String> destiny;
    private @FXML
    TableColumn<Flight, String> dateToFlight;
    private @FXML
    TableColumn<Flight, String> timeToFlight;
    private @FXML
    TableColumn<Flight, String> takenTime;
    private @FXML
    TableColumn<Flight, String> numberOfSoldTicket;
    private @FXML
    TableColumn<Flight, String> airplaneID;

    //this variable check that manager information controller load this page or do not
    private boolean isManager = false;
    //this variable check that passenger page controller load this page or do not
    private boolean isPassenger = false;
    //if isPassenger was true this variable save passenger username
    private String saveUsername;

    public void setPassenger(boolean passenger) {
        isPassenger = passenger;
    }

    public void setSaveUsername(String saveUsername) {
        this.saveUsername = saveUsername;
    }

    //if isManager was true this variable save id of airplane
    private String saveAirplaneID;
    //make window movable
    private double xOffset = 0;
    private double yOffset = 0;

    public void setCheck(boolean check) {
        this.isManager = check;
    }

    public void setSaveAirplaneID(String saveAirplaneID) {
        this.saveAirplaneID = saveAirplaneID;
    }

    //set on action for add button
    public void setOnActionAddBTN(ActionEvent event) {
        Stage stage = (Stage) addBTN.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Flight/AddFlightPage.fxml"));
        AnchorPane root = null;
        try {
            root = (AnchorPane) loader.load();
            AddFlightPageController controller = loader.getController();
            controller.setTableView(flightTableView);
            controller.setIsEnterParticularAirplaneID(isManager);
            controller.setSaveAirplaneID(saveAirplaneID);
        } catch (IOException e) {
            e.printStackTrace();
        }

        stage = new Stage(StageStyle.DECORATED);


        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        Stage finalStage = stage;
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                finalStage.setX(event.getScreenX() - xOffset);
                finalStage.setY(event.getScreenY() - yOffset);
            }
        });

        stage.setScene(new Scene(root));
        stage.show();
    }

    //set on action for remove button
    public void setOnActionRemoveBTN(ActionEvent event) {
        //get all information from table view
        ObservableList<Flight> allFlights , selectionFlight = null;
        allFlights = flightTableView.getItems();
        try {
            //get selection row from table view
            selectionFlight = flightTableView.getSelectionModel().getSelectedItems();
        }catch (NullPointerException e){
            System.out.println("Please select a row " + e.getMessage());
        }

        //find all related passengers with this flightID
        for (int i = 0; i < Ticket.tickets.size(); i++) {
            if (Ticket.tickets.get(i).getFlightID().equals(selectionFlight.get(0).getID())) {
                for (int j = 0; j < Ticket.tickets.get(i).getPassengers().size(); j++) {
                    Database.removeInformation(new Passenger(),Ticket.tickets.get(i).getPassengers().get(j).getUserName());
                }
            }
        }
        for (int i = 0; i < selectionFlight.size(); i++) {
            //find selection object in array list to remove it from database too
            for (int j = 0; j < Flight.flights.size(); j++) {
                if (selectionFlight.get(i).getID().equals(Flight.flights.get(j).getID())) {
                    Database.removeInformation(new Flight(), Flight.flights.get(j).getID());
                    Flight.flights.remove(j);
                    break;
                }
            }
        }

        //update the related array list
        Database.getInformation(new Passenger());
        Database.getInformation(new Ticket());
        Database.getInformation(new TicketsAndPassengers());
        //remove the row from table view
        //update lists
        Main.setPassengers();
        Main.setTicketsAndPassengers();
        Main.setTickets();
        Main.setFlights();
        selectionFlight.forEach(allFlights::remove);
    }

    //set on action for related passengers button
    public void setOnActionRelatedPassengersBTN(ActionEvent event) {
        //get current window
        Stage stage = (Stage) relatedPassengersBTN.getScene().getWindow();
        //get selected row of flight table view
        ObservableList<Flight> selectedFlight;
        selectedFlight = flightTableView.getSelectionModel().getSelectedItems();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Passenger/PassengerInformationPage.fxml"));
        BorderPane root = null;
        try {
            root = (BorderPane) loader.load();
            PassengerInformationPageController controller = loader.getController();
            controller.setEnterParticularFlightID(true);
            controller.setSaveAirplaneID(selectedFlight.get(0).getAirplaneID());
            controller.setSaveFlightID(selectedFlight.get(0).getID());
        } catch (IOException e) {
            e.printStackTrace();
        }

        stage = new Stage(StageStyle.DECORATED);


        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        Stage finalStage = stage;
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                finalStage.setX(event.getScreenX() - xOffset);
                finalStage.setY(event.getScreenY() - yOffset);
            }
        });

        stage.setScene(new Scene(root));
        stage.show();

    }

    //set on action for related tickets button
    public void setOnActionRelatedTicketsBTN(ActionEvent event) {
        if (isPassenger) {
            //create stage and set style
            Stage stage = new Stage(StageStyle.DECORATED);
            //get selected row of flight table view
            ObservableList<Flight> selectedFlight;
            selectedFlight = flightTableView.getSelectionModel().getSelectedItems();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Ticket/BuyTicketPage.fxml"));
            BorderPane root = null;
            try {
                //create root
                root = (BorderPane) loader.load();
                //get controller
                BuyTicketPageController controller = loader.getController();
                controller.setSaveUsername(saveUsername);
                controller.setFlightID(selectedFlight.get(0).getID());
                controller.setEnterParticularFlightID(true);
                controller.setAirplaneID(selectedFlight.get(0).getAirplaneID());
            } catch (IOException e) {
                e.printStackTrace();
            }

            //get mouse position
            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });

            //set mouse position
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    stage.setX(event.getScreenX() - xOffset);
                    stage.setY(event.getScreenY() - yOffset);
                }
            });

            stage.setScene(new Scene(root));
            stage.show();

        } else {
            //create stage and set style
            Stage stage = new Stage(StageStyle.DECORATED);
            //get selected row of flight table view
            ObservableList<Flight> selectedFlight;
            selectedFlight = flightTableView.getSelectionModel().getSelectedItems();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Ticket/TicketInformationPage.fxml"));
            BorderPane root = null;
            try {
                //create root
                root = (BorderPane) loader.load();
                TicketInformationPageController controller = loader.getController();
                controller.setEnterParticularFlightID(true);
                controller.setSaveAirplaneID(selectedFlight.get(0).getAirplaneID());
                controller.setSaveFlightID(selectedFlight.get(0).getID());
            } catch (IOException e) {
                e.printStackTrace();
            }



            //get mouse position
            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });

            //set mouse position
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    stage.setX(event.getScreenX() - xOffset);
                    stage.setY(event.getScreenY() - yOffset);
                }
            });

            stage.setScene(new Scene(root));
            stage.show();

        }
    }

    //set on action for exit button
    public void setOnActionExitBTN(ActionEvent event) {
        Stage stage = (Stage) exitBTN.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //make all column ready to set value on them
        id.setCellValueFactory(new PropertyValueFactory<>("iD"));
        origin.setCellValueFactory(new PropertyValueFactory<>("origin"));
        destiny.setCellValueFactory(new PropertyValueFactory<>("destiny"));
        dateToFlight.setCellValueFactory(new PropertyValueFactory<>("dateToFlight"));
        dateToFlight.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getDateToFight()));
        timeToFlight.setCellValueFactory(new PropertyValueFactory<>("timeToFlight"));
        timeToFlight.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getTimeToFight()));
        takenTime.setCellValueFactory(new PropertyValueFactory<>("takenTime"));
        airplaneID.setCellValueFactory(new PropertyValueFactory<>("airplaneID"));
        numberOfSoldTicket.setCellValueFactory(new PropertyValueFactory<>("numberOfSoldTicket"));

        //below line is for prevent of getting null variables
        Platform.runLater(() -> {
            if (isManager) {
                //here we find all flights with given airplane id
                for (int i = 0; i < Flight.flights.size(); i++) {
                    if (Flight.flights.get(i).getAirplaneID().equals(saveAirplaneID)) {
                        flightTableView.getItems().add(Flight.flights.get(i));
                    }
                }
            } else{
                //here we was entered by passenger
                  if (isPassenger){
                    relatedPassengersBTN.setDisable(true);
                    addBTN.setDisable(true);
                    removeBTN.setDisable(true);
                    flightTableView.setEditable(false);

                }
                //if we wasn't entered by show button
                for (int i = 0; i < Flight.flights.size(); i++) {
                    flightTableView.getItems().add(Flight.flights.get(i));
                }

            }

        });



        editTableColumn();


    }


    //this method make table view editable
    private void editTableColumn() {

        //make origin field editable
        origin.setCellFactory(TextFieldTableCell.forTableColumn());
        origin.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Flight> selectedItem;
            selectedItem = flightTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getID();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setOrigin(event.getNewValue());
            //change value in array list and database
            updateManagers(uniqValue, event.getNewValue(), "origin");
        });


        //make destiny field editable
        destiny.setCellFactory(TextFieldTableCell.forTableColumn());
        destiny.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Flight> selectedItem;
            selectedItem = flightTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getID();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setDestiny(event.getNewValue());
            //change value in array list and database
            updateManagers(uniqValue, event.getNewValue(), "destiny");
        });

        //make dateToFlight field editable
        dateToFlight.setCellFactory(TextFieldTableCell.forTableColumn());
        dateToFlight.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isDate = false;
            isDate = Check.checkDate(event.getNewValue());

            if (isDate) {

                //get selected row from table view
                ObservableList<Flight> selectedItem;
                selectedItem = flightTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getID();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setDateToFight(event.getNewValue());


                //change value in array list and database
                updateManagers(uniqValue, event.getNewValue(), "dateToFlight");

            } else {
                createErrorWindow("Invalid date input");
            }
        });

        //make timeToFlight field editable
        timeToFlight.setCellFactory(TextFieldTableCell.forTableColumn());
        timeToFlight.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isTime = false;
            isTime = Check.checkTime(event.getNewValue());

            if (isTime) {

                //get selected row from table view
                ObservableList<Flight> selectedItem;
                selectedItem = flightTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getID();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setTimeToFight(event.getNewValue());
                //change value in array list and database
                updateManagers(uniqValue, event.getNewValue(), "timeToFlight");

            } else {
                createErrorWindow("Invalid time input");
            }
        });

        //make takenTime field editable
        takenTime.setCellFactory(TextFieldTableCell.forTableColumn());
        takenTime.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isTime = false;
            isTime = Check.checkTime(event.getNewValue());

            if (isTime) {

                //get selected row from table view
                ObservableList<Flight> selectedItem;
                selectedItem = flightTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getID();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setTakenTime(event.getNewValue());
                //change value in array list and database
                updateManagers(uniqValue, event.getNewValue(), "takenTime");

            } else {
                createErrorWindow("Invalid time input");
            }
        });

        //make numberOfSoldTicket field editable
        numberOfSoldTicket.setCellFactory(TextFieldTableCell.forTableColumn());
        numberOfSoldTicket.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isNumber = false;
            isNumber = Check.checkNumber(event.getNewValue());
            if (isNumber) {
                //get selected row from table view
                ObservableList<Flight> selectedItem;
                selectedItem = flightTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getID();
                //update value in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setNumberOfSoldTicket(event.getNewValue());
                //change value in array list and database
                updateManagers(uniqValue, event.getNewValue(), "numberOfSoldTicket");
            } else {
                createErrorWindow("Please enter number");
            }
        });



        flightTableView.setEditable(true);
    }

    //this method will be always make array list of Flight update
    private void updateManagers(String ID, String newValue, String property) {

        //change old origin with new one
        if (property.equals("origin")) {
            for (int i = 0; i < Flight.flights.size(); i++) {
                if (Flight.flights.get(i).getID().equals(ID)) {
                    Flight.flights.get(i).setOrigin(newValue);
                    break;
                }
            }
        }

        //change old destiny with new one
        if (property.equals("destiny")) {
            for (int i = 0; i < Flight.flights.size(); i++) {
                if (Flight.flights.get(i).getID().equals(ID)) {
                    Flight.flights.get(i).setDestiny(newValue);
                    break;
                }
            }
        }

        //change old dateToFlight with new one
        if (property.equals("dateToFlight")) {
            for (int i = 0; i < Flight.flights.size(); i++) {
                if (Flight.flights.get(i).getID().equals(ID)) {
                    Flight.flights.get(i).setDateToFight(newValue);
                    break;
                }
            }
        }

        //change old timeToFlight with new one
        if (property.equals("timeToFlight")) {
            for (int i = 0; i < Flight.flights.size(); i++) {
                if (Flight.flights.get(i).getID().equals(ID)) {
                    Flight.flights.get(i).setTimeToFight(newValue);
                    break;
                }
            }
        }

        //change old takenTime with new one
        if (property.equals("takenTime")) {
            for (int i = 0; i < Flight.flights.size(); i++) {
                if (Flight.flights.get(i).getID().equals(ID)) {
                    Flight.flights.get(i).setTakenTime(newValue);
                    break;
                }
            }
        }

        //change old numberOfSoldTicket with new one
        if (property.equals("numberOfSoldTicket")) {
            for (int i = 0; i < Flight.flights.size(); i++) {
                if (Flight.flights.get(i).getID().equals(ID)) {
                    Flight.flights.get(i).setNumberOfSoldTicket(newValue);
                    break;
                }
            }
        }

        //change old airplaneID with new one
        if (property.equals("airplaneID")) {
            for (int i = 0; i < Flight.flights.size(); i++) {
                if (Flight.flights.get(i).getID().equals(ID)) {
                    Flight.flights.get(i).setAirplaneID(newValue);
                    break;
                }
            }
        }

        //update Flight table in database
        for (int i = 0; i < Flight.flights.size(); i++) {
            Database.updateInformation(Flight.flights.get(i),Flight.flights.get(i).getID());
        }
    }

    //this method create an error window with particular message
    private void createErrorWindow(String message) {
        ErrorPageController.text = message;
        ErrorPageController.createErrorWindow();
    }


}
