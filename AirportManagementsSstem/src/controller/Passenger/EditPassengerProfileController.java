package controller.Passenger;

import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import model.Passenger;

import java.net.URL;
import java.util.ResourceBundle;

public class EditPassengerProfileController implements Initializable {
    private @FXML TableColumn<Passenger, String> id;
    private @FXML TableColumn<Passenger, String> name;
    private @FXML TableColumn<Passenger, String> lastName;
    private @FXML TableColumn<Passenger, String> username;
    private @FXML TableColumn<Passenger, String> password;
    private @FXML TableColumn<Passenger, String> email;
    private @FXML TableColumn<Passenger, String> phoneNumber;
    private @FXML TableColumn<Passenger, String> credit;
    private @FXML TableView<Passenger> passengerTableView;

    //save username of passenger
    private String saveUsername;

    public void setSaveUsername(String saveUsername) {
        this.saveUsername = saveUsername;
    }

    //this method make table view editable
    private void editTableColumn() {

        //make name field editable
        name.setCellFactory(TextFieldTableCell.forTableColumn());
        name.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Passenger> selectedItem;
            selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setName(event.getNewValue());
            //change value in array list and database
            updateEmployee(uniqValue, event.getNewValue(), "name");

        });


        //make last name field editable
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Passenger> selectedItem;
            selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setLastName(event.getNewValue());
            //change value in array list and database
            updateEmployee(uniqValue, event.getNewValue(), "lastName");

        });

        //make username field editable
        username.setCellFactory(TextFieldTableCell.forTableColumn());
        username.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isUsername = false;
            isUsername = Check.checkUsernamePassenger(event.getNewValue());

            if (isUsername) {

                //get selected row from table view
                ObservableList<Passenger> selectedItem;
                selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setUserName(event.getNewValue());

                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "username");
            } else {
                createErrorWindow("This username already exists ");
            }
        });

        //make phone number field editable
        phoneNumber.setCellFactory(TextFieldTableCell.forTableColumn());
        phoneNumber.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPhoneNumber = false;
            isPhoneNumber = Check.checkPhoneNumber(event.getNewValue());

            if (isPhoneNumber) {

                //get selected row from table view
                ObservableList<Passenger> selectedItem;
                selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPhoneNumber(event.getNewValue());
                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "phoneNumber");

            } else {
                createErrorWindow("Invalid phone number");
            }
        });


        //make email field editable
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isEmail = false;
            isEmail = Check.checkEmailPassenger(event.getNewValue());
            if (isEmail) {

                //get selected row from table view
                ObservableList<Passenger> selectedItem;
                selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setEmail(event.getNewValue());
                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "email");

            } else {
                createErrorWindow("Invalid email");
            }
        });

        passengerTableView.setEditable(true);


    }
    //this method create an error window with particular message
    private void createErrorWindow(String message) {
        ErrorPageController.text = message;
        ErrorPageController.createErrorWindow();
    }



    //this method will be always make array list of Employee update
    private void updateEmployee(String username, String newValue, String property) {

        //change old name with new name
        if (property.equals("name")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setName(newValue);
                    break;
                }
            }
        }

        //change old last name with new last name
        if (property.equals("lastName")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setLastName(newValue);
                    break;
                }
            }
        }

        //change old username with new username
        if (property.equals("username")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setUserName(newValue);
                    username = newValue;
                    break;
                }
            }
        }


        //change old phone number with new phone number
        if (property.equals("phoneNumber")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setPhoneNumber(newValue);
                    break;
                }
            }
        }


        //change old email with new email
        if (property.equals("email")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setEmail(newValue);
                    break;
                }
            }
        }

        //update Passenger table in database
        for (int i = 0; i < Passenger.passengers.size(); i++) {
            Database.updateInformation(Passenger.passengers.get(i),Passenger.passengers.get(i).getID());
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //below line is for prevent of getting null variables
        Platform.runLater(() ->{
            //add the determined passenger
            for (int i = 0; i < Passenger.passengers.size(); i++) {
            if(Passenger.passengers.get(i).getUserName().equals(saveUsername)) {
                //we found the passenger
                passengerTableView.getItems().add(Passenger.passengers.get(i));
            }
        }});

        if (Passenger.passengers.size() != 0) {

            id.setCellValueFactory(new PropertyValueFactory<>("iD"));
            name.setCellValueFactory(new PropertyValueFactory<>("name"));
            lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
            username.setCellValueFactory(new PropertyValueFactory<>("userName"));
            password.setCellValueFactory(new PropertyValueFactory<>("password"));
            email.setCellValueFactory(new PropertyValueFactory<>("email"));
            phoneNumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
            credit.setCellValueFactory(new PropertyValueFactory<>("credit"));


        }
        editTableColumn();


    }
}
