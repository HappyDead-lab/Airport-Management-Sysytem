package controller.Passenger;

import Main.Main;
import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import model.Flight;
import model.Passenger;
import model.TicketsAndPassengers;

import java.net.URL;
import java.util.ResourceBundle;

public class PassengerInformationPageController implements Initializable {
    private @FXML TableColumn<Passenger, String> id;
    private @FXML TableColumn<Passenger, String> name;
    private @FXML TableColumn<Passenger, String> lastName;
    private @FXML TableColumn<Passenger, String> username;
    private @FXML TableColumn<Passenger, String> password;
    private @FXML TableColumn<Passenger, String> email;
    private @FXML TableColumn<Passenger, String> phoneNumber;
    private @FXML TableColumn<Passenger, String> credit;
    @FXML private TableView<Passenger> passengerTableView;
    @FXML private Button exitBTN;
    //if we was entered by related passenger button
    private boolean isEnterParticularFlightID = false;
    //save flight id if we was entered by show button
    private String saveAirplaneID;

    public void setEnterParticularFlightID(boolean enterParticularFlightID) {
        isEnterParticularFlightID = enterParticularFlightID;
    }

    public void setSaveAirplaneID(String saveAirplaneID) {
        this.saveAirplaneID = saveAirplaneID;
    }

    public void setSaveFlightID(String saveFlightID) {
        this.saveFlightID = saveFlightID;
    }

    private String saveFlightID;

    //set on action for exit button
    public void setOnActionExitBTN(ActionEvent event) {
        Stage stage = (Stage) exitBTN.getScene().getWindow();
        stage.close();
    }


    //set on action for remove button
    public void setOnActionRemoveBTN(ActionEvent event) {

        //get all information from table view
        ObservableList<Passenger> allPassengers, selectionPassenger = null;
        allPassengers = passengerTableView.getItems();
        try {
            //get selection row from table view
            selectionPassenger = passengerTableView.getSelectionModel().getSelectedItems();
        }catch (NullPointerException e){
            System.out.println("Please select a row " + e.getMessage());
        }


            for (int i = 0; i < Flight.flights.size(); i++) {
                for (int j = 0; j < Flight.flights.get(i).getPassengers().size(); j++) {
                    if(Flight.flights.get(i).getPassengers().get(j).getID().equals(selectionPassenger.get(0).getID())){
                        //save current number of sold tickets
                        int numberOfSoldTickets = Integer.parseInt(Flight.flights.get(i).getNumberOfSoldTicket());
                        numberOfSoldTickets--;
                        //set new number of sold tickets
                        Flight.flights.get(i).setNumberOfSoldTicket(Integer.toString(numberOfSoldTickets));
                        //update Flight table in database
                        Database.updateInformation(Flight.flights.get(i),Flight.flights.get(i).getID());
                    }
                }
            }
            for (int i = 0; i < selectionPassenger.size(); i++) {
            //find selection object in array list to remove it from database too
            for (int j = 0; j < Passenger.passengers.size(); j++) {
                if (selectionPassenger.get(i).getUserName().equals(Passenger.passengers.get(j).getUserName())) {
                    //found it
                    Database.removeInformation(new Passenger(), Passenger.passengers.get(j).getUserName());
                    Passenger.passengers.remove(j);
                    break;
                }
            }
        }




        //update relates tables in database
        Database.getInformation(new TicketsAndPassengers());
        //update lists
        Main.setPassengers();
        Main.setTickets();
        Main.setTicketsAndPassengers();
        //remove it from table view
        selectionPassenger.forEach(allPassengers::remove);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (Passenger.passengers.size() != 0) {

            id.setCellValueFactory(new PropertyValueFactory<>("iD"));
            name.setCellValueFactory(new PropertyValueFactory<>("name"));
            lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
            username.setCellValueFactory(new PropertyValueFactory<>("userName"));
            password.setCellValueFactory(new PropertyValueFactory<>("password"));
            email.setCellValueFactory(new PropertyValueFactory<>("email"));
            phoneNumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
            credit.setCellValueFactory(new PropertyValueFactory<>("credit"));
            //below line is for prevent of getting null variables
            Platform.runLater(() ->{
                if(isEnterParticularFlightID){
                    for (int i = 0; i < Passenger.passengers.size(); i++) {
                        for (int j = 0; j < Passenger.passengers.get(i).getTickets().size(); j++) {
                            if( Passenger.passengers.get(i).getTickets().get(j).getFlightID().equals(saveFlightID)){
                                passengerTableView.getItems().add(Passenger.passengers.get(i));
                            }
                        }
                    }

                }else {
                    //if we wasn't entered by show button
                    for (int i = 0; i < Passenger.passengers.size(); i++) {
                        passengerTableView.getItems().add(Passenger.passengers.get(i));
                    }
                }
            });

        }
        editTableColumn();
    }

    //this method make table view editable
    public void editTableColumn() {

        //make name field editable
        name.setCellFactory(TextFieldTableCell.forTableColumn());
        name.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Passenger> selectedItem;
            selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setName(event.getNewValue());
            //change value in array list and database
            updatePassenger(uniqValue, event.getNewValue(), "name");

        });


        //make last name field editable
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Passenger> selectedItem;
            selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setLastName(event.getNewValue());
            //change value in array list and database
            updatePassenger(uniqValue, event.getNewValue(), "lastName");

        });

        //make username field editable
        username.setCellFactory(TextFieldTableCell.forTableColumn());
        username.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isUsername = false;
            isUsername = Check.checkUsernamePassenger(event.getNewValue());

            if (isUsername) {

                //get selected row from table view
                ObservableList<Passenger> selectedItem;
                selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setUserName(event.getNewValue());
                //change value in array list and database
                updatePassenger(uniqValue, event.getNewValue(), "username");
            } else {
                createErrorWindow("This username already exists ");
            }
        });

        //make password field editable
        password.setCellFactory(TextFieldTableCell.forTableColumn());
        password.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPassword = false;
            isPassword = Check.checkPassword(event.getNewValue());
            if (isPassword) {


                //get selected row from table view
                ObservableList<Passenger> selectedItem;
                selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();


                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPassword(event.getNewValue());


                //change value in array list and database
                updatePassenger(uniqValue, event.getNewValue(), "password");
            } else {
                createErrorWindow("It is less than 5 character");
            }
        });

        //make phone number field editable
        phoneNumber.setCellFactory(TextFieldTableCell.forTableColumn());
        phoneNumber.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPhoneNumber = false;
            isPhoneNumber = Check.checkPhoneNumber(event.getNewValue());

            if (isPhoneNumber) {

                //get selected row from table view
                ObservableList<Passenger> selectedItem;
                selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPhoneNumber(event.getNewValue());
                //change value in array list and database
                updatePassenger(uniqValue, event.getNewValue(), "phoneNumber");

            } else {
                createErrorWindow("Invalid phone number");
            }
        });


        //make email field editable
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isEmail = false;
            isEmail = Check.checkEmailPassenger(event.getNewValue());
            if (isEmail) {

                //get selected row from table view
                ObservableList<Passenger> selectedItem;
                selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setEmail(event.getNewValue());
                //change value in array list and database
                updatePassenger(uniqValue, event.getNewValue(), "email");

            } else {
                createErrorWindow("Invalid email");
            }
        });

        //make salary field editable
        credit.setCellFactory(TextFieldTableCell.forTableColumn());
        credit.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isCredit = false;
            isCredit = Check.checkNumber(event.getNewValue());
            if (isCredit) {

                //get selected row from table view
                ObservableList<Passenger> selectedItem;
                selectedItem = passengerTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setCredit(event.getNewValue());
                //change value in array list and database
                updatePassenger(uniqValue, event.getNewValue(), "credit");

            } else {
                createErrorWindow("Please enter number");
            }
        });


        passengerTableView.setEditable(true);
    }
    //this method create an error window with particular message
    private void createErrorWindow(String message) {
        ErrorPageController.text = message;
        ErrorPageController.createErrorWindow();
    }



    //this method will be always make array list of Passenger update
    private void updatePassenger(String username, String newValue, String property) {

        //change old name with new name
        if (property.equals("name")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setName(newValue);
                }
            }
        }

        //change old last name with new last name
        if (property.equals("lastName")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setLastName(newValue);
                }
            }
        }

        //change old username with new username
        if (property.equals("username")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setUserName(newValue);
                }
            }
        }

        //change old password with new password
        if (property.equals("password")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setPassword(newValue);
                }
            }
        }

        //change old phone number with new phone number
        if (property.equals("phoneNumber")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setPhoneNumber(newValue);
                }
            }
        }


        //change old email with new email
        if (property.equals("email")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setEmail(newValue);
                }
            }
        }

        //change old salary with new salary
        if (property.equals("credit")) {
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if (Passenger.passengers.get(i).getUserName().equals(username)) {
                    Passenger.passengers.get(i).setCredit(newValue);
                }
            }
        }

        //update Passenger table in database
        for (int i = 0; i < Passenger.passengers.size(); i++) {
            Database.updateInformation(Passenger.passengers.get(i),Passenger.passengers.get(i).getID());
        }

    }

}
