package controller.Passenger;

import controller.*;
import controller.Flight.FlightInformationPageController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import model.Passenger;

import java.io.IOException;

public class PassengerPageController {
    private @FXML Button editProfileBTN;
    private @FXML Button changePassBTN;
    private @FXML Button changeBTN;
    private @FXML PasswordField passwordPSF;
    private @FXML PasswordField confirmationPSF;
    private @FXML Label successfulLBL;

    //this variables help that make window movable
    private double xOffset = 0;
    private double yOffset = 0;

    //set on action for buying and cancelling ticket
    public void setOnActionBuyingAndCancellingBTN(ActionEvent event){
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Flight/FlightInformationPage.fxml"));
        //create root
        BorderPane root = null;
        try {
            root = (BorderPane) loader.load();
            //get controller
            FlightInformationPageController controller = loader.getController();
            controller.setSaveUsername(saveUsername);
            controller.setPassenger(true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //get position of mouse
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set position of mouse
        root.setOnMouseDragged(event1 -> {
            stage.setX(event1.getScreenX() - xOffset);
            stage.setY(event1.getScreenY() - yOffset);
        });
        //set scene
        stage.setScene(new Scene(root));
        stage.show();


    }


    //this boolean is for personal profile management button
    private boolean checkPersonalBTN = false;

    //this boolean is for change password button
    private boolean checkChangeBTN = false;

    //this variable for save input username that passenger login with it
    private  String saveUsername;

    public  void setUsername(String username) {
        saveUsername = username;
    }

    //set on action for edit profile button
    public void setOnActionEditProfileBTN(ActionEvent event){
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);
        //create root
        BorderPane root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Passenger/EditPassengerProfile.fxml"));
        try {
            //load page
            root = (BorderPane)loader.load();
            //get controller
            EditPassengerProfileController controller = loader.getController();
            controller.setSaveUsername(saveUsername);

        } catch (IOException e) {
            e.printStackTrace();
        }

        //get position of mouse
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set position of mouse
        root.setOnMouseDragged(event1 -> {
            stage.setX(event1.getScreenX() - xOffset);
            stage.setY(event1.getScreenY() - yOffset);
        });

        stage.setScene(new Scene(root));
        stage.show();
    }

    //set on action for personal profile management button
    public void setOnActionPersonalBTN(ActionEvent event){
        if(!checkPersonalBTN) {
            //for first click
            visibleBTN(editProfileBTN,0.50);
            visibleBTN(changePassBTN,0.75);
            checkPersonalBTN = true;
        }else if (checkPersonalBTN){
            //for second click
            invisibleLBL(successfulLBL,0.50);
            invisibleBTN(changePassBTN,1.50);
            invisibleBTN(changeBTN,0.95);
            invisiblePSF(confirmationPSF,1.25);
            invisiblePSF(passwordPSF,1.40);
            invisibleBTN(editProfileBTN,1.60);
            checkPersonalBTN = false;
        }

    }

    //set on action for change password button
    public void setOnActionChangePassBTN(ActionEvent event) {
        if (!checkChangeBTN) {
            //for first click
            visiblePSF(passwordPSF,0.75);
            visiblePSF(confirmationPSF,0.95);
            visibleBTN(changeBTN,1.25);
            checkChangeBTN = true;
        }else if(checkChangeBTN){
            //for second click
            invisibleLBL(successfulLBL,0.50);
            invisibleBTN(changeBTN,0.95);
            invisiblePSF(confirmationPSF,1.25);
            invisiblePSF(passwordPSF,1.40);
            checkChangeBTN = false;
        }
    }

    //set on action for change button
    public void setOnActionChangeBTN(ActionEvent event) {
        Stage stage1;
        //show appropriate error
        if (passwordPSF.getText().equals("") || confirmationPSF.getText().equals("")) {
            //if at least one of field is empty

            //set message of error
            ErrorPageController.text = "Please fill all of the fields";
            ErrorPageController.createErrorWindow();
        } else if (!(passwordPSF.getText().equals(confirmationPSF.getText()))) {
            //if new password is not equal to its confirmation

            //set message of error
            ErrorPageController.text = "Please enter correct confirmation";
            ErrorPageController.createErrorWindow();
        } else {
            //change password of passenger
            //this variable check input password is really a password or not
            boolean isPass = false;

            isPass = Check.checkPassword(passwordPSF.getText());

            if (isPass) {
                //here we find the user that enter an change password
                for (int i = 0; i < Passenger.passengers.size(); i++) {
                    if (Passenger.passengers.get(i).getUserName().equals(saveUsername)) {
                        //find it
                        Passenger.passengers.get(i).setPassword(passwordPSF.getText());
                        Database.changePassword(new Passenger(), saveUsername, passwordPSF.getText());
                        break;
                    }
                }

                //clear password fields
                passwordPSF.clear();
                confirmationPSF.clear();


                visibleLBL(successfulLBL, 0.1);

            }else {
                //set error message
                ErrorPageController.text = "It is less than 5 characters";
                ErrorPageController.createErrorWindow();
            }
        }
    }
    //set on action for send message button
    public void setOnActionSendMessageBTN(ActionEvent event){
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MessagePage.fxml"));
        AnchorPane root = null;
        try {
            //load page
            root = (AnchorPane) loader.load();
            //get controller
            MessagePageController controller = loader.getController();
            //determine user is passenger
            controller.setPassenger(true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //get position of mouse
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        //set position of mouse
        Stage finalStage = stage;
        root.setOnMouseDragged(event1 -> {
            finalStage.setX(event1.getScreenX() - xOffset);
            finalStage.setY(event1.getScreenY() - yOffset);
        });

        //set scene
        stage.setScene(new Scene(root));
        stage.show();
    }

    //set on action for increment of credit button
    public void setOnActionIncrementOfCreditBTN(ActionEvent event){
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);
        //create root
        AnchorPane root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/IncrementOfCreditPage.fxml"));
        //load page
        try {
            root = loader.load();
            //get controller
            IncrementOfCreditPageController controller = loader.getController();
            controller.setSaveUsername(saveUsername);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //get position of mouse
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        //set position of mouse
        Stage finalStage = stage;
        root.setOnMouseDragged(event1 -> {
            finalStage.setX(event1.getScreenX() - xOffset);
            finalStage.setY(event1.getScreenY() - yOffset);
        });

        //set scene
        stage.setScene(new Scene(root));
        stage.show();

    }

    //this method visible a button
    private void visibleBTN(Button button , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                button.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //this method invisible a button
    private void invisibleBTN(Button button , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it invisible after a few second
                                button.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

    //this method visible a label
    private void visibleLBL(Label label , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //this method visible a label
    private void invisibleLBL(Label label , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it invisible after a few second
                                label.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

    //this method visible a password field
    private void visiblePSF(PasswordField passwordField , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                passwordField.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

    //this method invisible a password field
    private void invisiblePSF(PasswordField passwordField , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it invisible after a few second
                                passwordField.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }
}
