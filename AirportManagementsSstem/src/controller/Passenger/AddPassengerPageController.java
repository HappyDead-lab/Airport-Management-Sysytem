package controller.Passenger;

import Main.Main;
import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Passenger;
import java.net.URL;
import java.util.ResourceBundle;

public class AddPassengerPageController implements Initializable {
    @FXML private TextField firstNameTXF;
    @FXML private TextField lastNameTXF;
    @FXML private TextField phoneNumberTXF;
    @FXML private TextField usernameTXF;
    @FXML private TextField emailTXF;
    @FXML private TextField creditTXF;
    @FXML private PasswordField passwordPSF;
    @FXML private PasswordField confirmationPSF;
    @FXML private Label invalidPhoneNumberLBL;
    @FXML private Label invalidEmailLBL;
    @FXML private Label invalidUsername;
    @FXML private Label invalidCreditLBL;
    @FXML private Label invalidPassLBL;
    @FXML private Button addBTN;
    @FXML private Button cancelBTN;
    private TableView<Passenger> tableView ;



    //set on action for cancel button
    public void setOnActionCancelBTN(ActionEvent event){
        //get current window
        Stage stage = (Stage)cancelBTN.getScene().getWindow();
        stage.close();
    }

    public void setTableView(TableView<Passenger> tableView) {
        this.tableView = tableView;
    }

    public void setOnActionAddBTN(ActionEvent event)  {
        //get current stage
        Stage stage = (Stage)addBTN.getScene().getWindow();

        if(firstNameTXF.getText().equals("") || lastNameTXF.getText().equals("") || phoneNumberTXF.getText().equals("")  || usernameTXF.getText().equals("") || emailTXF.getText().equals("") || creditTXF.getText().equals("") || passwordPSF.getText().equals("") || confirmationPSF.getText().equals("")){
            //show error if one of the fields be empty
            ErrorPageController.text = "Please fill all parts of the fields";
            ErrorPageController.createErrorWindow();
        }else if(!passwordPSF.getText().equals(confirmationPSF.getText())) {
            //show error if password field and confirmation aren't equal
            //set error message
            ErrorPageController.text = "Please enter correct confirmation";
            ErrorPageController.createErrorWindow();
        }else {
            //declare an instance
            Passenger passenger = new Passenger();

            //to check that some input value is correct or not
            boolean isPhoneNumber = false;
            boolean isUsername = false;
            boolean isEmail = false;
            boolean isCredit = false;
            boolean isPassword = false;


            //check input salary is valid or not
            isCredit = Check.checkNumber(creditTXF.getText());
            if(isCredit){
                invisible(invalidCreditLBL);
                passenger.setCredit(creditTXF.getText());
            }else {
                //visible error label
                visible(invalidCreditLBL);
            }

            //check input email is valid or not
            isEmail = Check.checkEmailPassenger(emailTXF.getText());
            if(isEmail){
                invisible(invalidEmailLBL);
                passenger.setEmail(emailTXF.getText());
            }else {
                //visible error label
                visible(invalidEmailLBL);
            }

            //check input username is valid or not
            isUsername = Check.checkUsernamePassenger(usernameTXF.getText());
            if(isUsername){
                invisible(invalidUsername);
                passenger.setUserName(usernameTXF.getText());
            }else{
                //visible error label
                visible(invalidUsername);
            }


            //check input phone number is valid or not
            isPhoneNumber = Check.checkPhoneNumber(phoneNumberTXF.getText());
            if(isPhoneNumber) {
                invisible(invalidPhoneNumberLBL);
                passenger.setPhoneNumber(phoneNumberTXF.getText());
            }else{
                //visible error label
                visible(invalidPhoneNumberLBL);
            }

            isPassword = Check.checkPassword(passwordPSF.getText());
            if(isPassword){
                invisible(invalidPassLBL);
                passenger.setPassword(passwordPSF.getText());
            }else{
                //visible error label
                visible(invalidPassLBL);
            }

            if(isEmail && isPhoneNumber && isUsername && isCredit && isPassword) {
                //invisible all labels
                invisible(invalidPassLBL);
                invisible(invalidPhoneNumberLBL);
                invisible(invalidUsername);
                invisible(invalidEmailLBL);
                invisible(invalidCreditLBL);

                //add remains information
                passenger.setName(firstNameTXF.getText());
                passenger.setLastName(lastNameTXF.getText());
                passenger.setPassword(passwordPSF.getText());
                passenger.setCredit(creditTXF.getText());


                //save information on database
                Database.setInformation(passenger);
                //get ID from database
                Database.getInformation(new Passenger());
                //set ID
                passenger.setID(Passenger.passengers.get(Passenger.passengers.size() - 1).getID());
                //update list of passengers for Flight class
                Main.setPassengers();
                //add information to table view at the moment
                if(tableView != null)
                    tableView.getItems().add(passenger);

                stage.close();
            }
        }
    }

    //make a label visible
    private void visible(Label label){
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //make a label invisible
    private void invisible(Label label){
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it invisible after a few second
                                label.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //below line is for prevent of getting null variables
        Platform.runLater(() -> {
        });
    }
}
