package controller;

import model.*;

import java.sql.*;

import static java.sql.DriverManager.getConnection;

public class Database {
    //here we save database address
    private static String url = "jdbc:sqlserver://HappyDead:1433;databaseName=Airport Management System;user=sa;password=abh123";

    public Database() {


    }


    //this method get information from program and insert it to database and array list from specified Class
    public static void setInformation(Object object) {
        Connection connection1 = null;
        try {
            //connect to database
            connection1 = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println("we are in database class in set information method " + e.getMessage());
        }

        String insertStr = "";
        if (object instanceof Manager) {
            //object is Manager
            Manager manager = (Manager) object;
            //insert information from program to Manager table
            insertStr = "INSERT INTO Manager (FirstName,LastName,Username,Password,Email,PhoneNumber,Address,Salary) VALUES(?,?,?,?,?,?,?,?)";

            try (PreparedStatement pst = connection1.prepareStatement(insertStr)) {
                //here we get information from program and insert it to database and array list from Manager Class
                pst.setString(1, manager.getName());
                pst.setString(2, manager.getLastName());
                pst.setString(3, manager.getUserName());
                pst.setString(4, manager.getPassword());
                pst.setString(5, manager.getEmail());
                pst.setString(6, manager.getPhoneNumber());
                pst.setString(7, manager.getAddress());
                pst.setString(8, manager.getSalary());
                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Manager table" + " " + e.getMessage());
            }
        } else if (object instanceof Employee) {

            //object is Employee
            Employee employee = new Employee();
            employee = (Employee) object;
            //insert information from program to Employee table
            insertStr = "INSERT INTO Employee (FirstName,LastName,Username,Password,Email,PhoneNumber,Address,Salary) VALUES(?,?,?,?,?,?,?,?)";

            try (PreparedStatement pst = connection1.prepareStatement(insertStr)) {
                //here we get information from program and insert it to database and array list from Employee Class
                pst.setString(1, employee.getName());
                pst.setString(2, employee.getLastName());
                pst.setString(3, employee.getUserName());
                pst.setString(4, employee.getPassword());
                pst.setString(5, employee.getEmail());
                pst.setString(6, employee.getPhoneNumber());
                pst.setString(7, employee.getAddress());
                pst.setString(8, employee.getSalary());
                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Employee table" + " " + e.getMessage());
            }
        } else if (object instanceof Passenger) {
            //object is Passenger
            Passenger passenger = (Passenger) object;
            //insert information from program to Passenger table
            insertStr = "INSERT INTO Passenger (FirstName,LastName,Username,Password,PhoneNumber,Email,Credit) VALUES(?,?,?,?,?,?,?)";

            try (PreparedStatement pst = connection1.prepareStatement(insertStr)) {
                //here we get information from program and insert it to database and array list from Passenger Class
                pst.setString(1, passenger.getName());
                pst.setString(2, passenger.getLastName());
                pst.setString(3, passenger.getUserName());
                pst.setString(4, passenger.getPassword());
                pst.setString(5, passenger.getPhoneNumber());
                pst.setString(6, passenger.getEmail());
                pst.setString(7, passenger.getCredit());
                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Passenger table" + " " + e.getMessage());
            }
        } else if (object instanceof Airplane) {
            //object is Airplane
            Airplane airplane = (Airplane) object;
            //insert information from program to Airplane table
            insertStr = "INSERT INTO Airplane ([Numbet of seats]) VALUES (?)";
            try (PreparedStatement pst = connection1.prepareStatement(insertStr)) {
                //here we get information from program and insert it to database and array list from Airplane Class
                pst.setString(1, airplane.getNumberOfSeats());
                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Airplane table" + " " + e.getMessage());
            }
        } else if (object instanceof Flight) {
            //object is flight
            Flight flight = (Flight) object;
            //insert information from program to Flight table
            insertStr = "INSERT INTO Flight (Origin,Destiny,[Date to flight],[Time to flight],[Number of sold Ticket],[Taken time],[AirplaneID]) VALUES(?,?,?,?,?,?,?)";
            try (PreparedStatement pst = connection1.prepareStatement(insertStr)) {
                //here we get information from program and insert it to database and array list from Flight Class
                pst.setString(1, flight.getOrigin());
                pst.setString(2, flight.getDestiny());
                pst.setString(3, flight.getDateToFight());
                pst.setString(4, flight.getTimeToFight());
                pst.setString(5, flight.getNumberOfSoldTicket());
                pst.setString(6, flight.getTakenTime());
                pst.setString(7, flight.getAirplaneID());

                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Flight table" + " " + e.getMessage());
            }
        } else if (object instanceof Ticket) {

            //object is ticket
            Ticket ticket = (Ticket) object;
            //insert information from program to Ticket table
            insertStr = "INSERT INTO Ticket (Price,[Price of cancelling],[FlightID]) VALUES (?,?,?)";
            try (PreparedStatement pst = connection1.prepareStatement(insertStr)) {
                //here we get information from program and insert it to database and array list from Ticket Class
                pst.setString(1, ticket.getPrice());
                pst.setString(2, ticket.getPriceOfCancelling());
                pst.setString(3, ticket.getFlightID());
                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Ticket table" + " " + e.getMessage());
            }

        } else if (object instanceof TicketsAndPassengers) {
            //object is ticketsAndPassengers
            TicketsAndPassengers ticketsAndPassengers;
            ticketsAndPassengers = (TicketsAndPassengers) object;
            //insert information from program to Ticket table
            insertStr = "INSERT INTO Tickets_And_Passengers (passengerID,ticketID) VALUES (?,?)";
            try (PreparedStatement pst = connection1.prepareStatement(insertStr)) {
                pst.setString(1, ticketsAndPassengers.getPassengerID());
                pst.setString(2, ticketsAndPassengers.getTicketID());
                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Tickets_And_Passengers table" + " " + e.getMessage());
            }
        }
    }

    //this method update information that may be change during execute program
    public static void updateInformation(Object object, String ID) {
        String updateStr = "";
        Connection connection1 = null;
        try {
            //connect to database
            connection1 = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println("we are in database class in update information method " + e.getMessage());
        }
        if (object instanceof Manager) {


            //object is Manager
            Manager manager = (Manager) object;
            //update information in Manager table in database
            updateStr = "UPDATE Manager SET FirstName = ?,LastName = ?,Username = ?,Password = ?,Email = ?,PhoneNumber = ?,Address = ?,Salary = ? WHERE ID = ?";

            try (PreparedStatement pst = connection1.prepareStatement(updateStr)) {
                //here we get information from program and update it in Manager table in database
                pst.setString(1, manager.getName());
                pst.setString(2, manager.getLastName());
                pst.setString(3, manager.getUserName());
                pst.setString(4, manager.getPassword());
                pst.setString(5, manager.getEmail());
                pst.setString(6, manager.getPhoneNumber());
                pst.setString(7, manager.getAddress());
                pst.setString(8, manager.getSalary());
                pst.setString(9, ID);
                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Manager table" + " " + e.getMessage());
            }

        } else if (object instanceof Passenger) {

            //object is Passenger
            Passenger passenger = (Passenger) object;
            //update information in Passenger table in database
            updateStr = "UPDATE Passenger SET FirstName = ?, LastName = ?, Username = ?, Password = ?, PhoneNumber = ?, Email = ?, Credit = ?  WHERE ID = ?";


            try (PreparedStatement pst = connection1.prepareStatement(updateStr)) {
                //here we get information from program and update it in Passenger table in database
                pst.setString(1, passenger.getName());
                pst.setString(2, passenger.getLastName());
                pst.setString(3, passenger.getUserName());
                pst.setString(4, passenger.getPassword());
                pst.setString(5, passenger.getPhoneNumber());
                pst.setString(6, passenger.getEmail());
                pst.setString(7, passenger.getCredit());
                pst.setString(8, ID);

                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Passenger table" + " " + e.getMessage());
            }
        } else if (object instanceof Employee) {
            //object is Employee
            Employee employee = (Employee) object;
            //update information in Employee table in database
            updateStr = "UPDATE Employee SET FirstName = ?,LastName = ?,Username = ?,Password = ?,Email = ?,PhoneNumber = ?,Address = ?,Salary = ? WHERE ID = ? ";

            try (PreparedStatement pst = connection1.prepareStatement(updateStr)) {
                //here we get information from program and update it in Employee table in database
                pst.setString(1, employee.getName());
                pst.setString(2, employee.getLastName());
                pst.setString(3, employee.getUserName());
                pst.setString(4, employee.getPassword());
                pst.setString(5, employee.getEmail());
                pst.setString(6, employee.getPhoneNumber());
                pst.setString(7, employee.getAddress());
                pst.setString(8, employee.getSalary());
                pst.setString(9, ID);
                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Employee table" + " " + e.getMessage());
            }
        } else if (object instanceof Flight) {

            //object is flight
            Flight flight = (Flight) object;
            //update information in Flight table in database
            updateStr = "UPDATE Flight SET Origin = ?,Destiny = ?,[Date to flight] = ?,[Time to flight] = ?,[Number of sold Ticket] = ?,[Taken time] = ?,[AirplaneID] = ? WHERE ID = ? ";
            try (PreparedStatement pst = connection1.prepareStatement(updateStr)) {
                //here we get information from program and update it in Flight table in database
                pst.setString(1, flight.getOrigin());
                pst.setString(2, flight.getDestiny());
                pst.setString(3, flight.getDateToFight());
                pst.setString(4, flight.getTimeToFight());
                pst.setString(5, flight.getNumberOfSoldTicket());
                pst.setString(6, flight.getTakenTime());
                pst.setString(7, flight.getAirplaneID());
                pst.setString(8, ID);

                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Flight table" + " " + e.getMessage());
            }
        } else if (object instanceof Ticket) {
            //object is ticket
            Ticket ticket = (Ticket) object;
            //update information in Ticket table in database
            updateStr = "UPDATE Ticket SET Price = ?,[Price of cancelling] = ?,[FlightID] = ? WHERE ID = ?";
            try (PreparedStatement pst = connection1.prepareStatement(updateStr)) {
                //here we get information from program and update it in Ticket table in database
                pst.setString(1, ticket.getPrice());
                pst.setString(2, ticket.getPriceOfCancelling());
                pst.setString(3, ticket.getFlightID());
                pst.setString(4, ID);
                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Ticket table" + " " + e.getMessage());
            }
        } else if (object instanceof Airplane) {

            //object is Airplane
            Airplane airplane = (Airplane) object;
            //update information in Airplane table in database
            updateStr = "UPDATE Airplane SET [Numbet of seats] = ? WHERE ID = ?";
            try (PreparedStatement pst = connection1.prepareStatement(updateStr)) {
                //here we get information from program and update it in Airplane table in database
                pst.setString(1, airplane.getNumberOfSeats());
                pst.setString(2, ID);
                int i = pst.executeUpdate();
            } catch (SQLException e) {
                System.out.println("You cannot insert information into Airplane table" + " " + e.getMessage());
            }
        }
    }

    //this method will remove information from database
    public static void removeInformation(Object object, String particularField) {
        Connection connection1 = null;
        try {
            //connect to database
            connection1 = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("it's happened in remove information method and connection has problem");
        }
        //if user be manager
        if (object instanceof Manager) {

            PreparedStatement statement = null;
            try {
                statement = connection1.prepareStatement("DELETE FROM Manager WHERE Username = ?");
                //here remove the manager from database
                statement.setString(1, particularField);
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                System.out.println("it's happened in remove information method and statement has problem");
            }

            //if user be employee
        } else if (object instanceof Employee) {

            PreparedStatement statement = null;
            try {
                statement = connection1.prepareStatement("DELETE FROM Employee WHERE Username = ?");
                //here remove the employee from database
                statement.setString(1, particularField);
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                System.out.println("it's happened in remove information method and statement has problem");
            }
            //if user be passenger
        } else if (object instanceof Passenger) {

            PreparedStatement statement = null;
            try {
                statement = connection1.prepareStatement("DELETE FROM Passenger WHERE Username = ?");
                //here remove the passenger from database
                statement.setString(1, particularField);
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                System.out.println("it's happened in remove information method and statement has problem");
            }
            //if object was a Airplane
        } else if (object instanceof Airplane) {

            PreparedStatement statement = null;
            try {
                statement = connection1.prepareStatement("DELETE FROM Airplane WHERE ID = ?");
                //here remove the airplane from database
                statement.setString(1, particularField);
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                System.out.println("it's happened in remove information method and statement has problem");
            }
            //if object was a Flight
        } else if (object instanceof Flight) {

            PreparedStatement statement = null;
            try {
                statement = connection1.prepareStatement("DELETE FROM Flight WHERE ID = ?");
                //here remove the flight from database
                statement.setString(1, particularField);
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                System.out.println("it's happened in remove information method and statement has problem");
            }

        } else if (object instanceof Ticket) {
            PreparedStatement statement = null;
            try {
                statement = connection1.prepareStatement("DELETE FROM Ticket WHERE ID = ?");
                //here remove the ticket from database
                statement.setString(1, particularField);
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                System.out.println("it's happened in remove information method and statement has problem");
            }
        } else if (object instanceof TicketsAndPassengers) {

            PreparedStatement statement = null;
            try {
                statement = connection1.prepareStatement("DELETE FROM Tickets_And_Passengers WHERE ticketID = ?");
                //here remove the relation between ticket and passenger from database
                statement.setString(1, particularField);
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                System.out.println("it's happened in remove information method and statement has problem");
            }
        }
    }

    //set new value when Passenger buy ticket
    public static void updateInformationTicket(String numberOfSoldTicket, String flightID, String newCredit, String passengerUsername) {
        String updateFlight = "UPDATE Flight SET [Number of sold Ticket] = ? WHERE ID = ?";
        String updateCredit = "UPDATE Passenger SET Credit = ? WHERE Username = ?";
        PreparedStatement statement = null;
        try {
            //connect to database
            Connection connection1 = DriverManager.getConnection(url);
            //update information of flight table
            statement = connection1.prepareStatement(updateFlight);
            statement.setString(1, numberOfSoldTicket);
            statement.setString(2, flightID);
            statement.executeUpdate();
            //update new credit for passenger
            statement = connection1.prepareStatement(updateCredit);
            statement.setString(1, newCredit);
            statement.setString(2, passengerUsername);
            statement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("this error happened in update information ticket method " + e.getMessage());
        }

    }


    // here we get information from database and insert it to array list from Manager class
    public static void getInformation(Object object) {
        if (object instanceof Manager) {
            Manager.managers.clear();
            ResultSet resultSet = null;
            String sql = "SELECT * FROM Manager";

            try {
                //connect to database and execute query
                Connection connection1 = DriverManager.getConnection(url);
                Statement s = connection1.createStatement();
                resultSet = s.executeQuery(sql);
            } catch (SQLException e) {
                System.out.println("we are in Database class in get information method " + e.getMessage());
            }

            //for save information that insert into Manager table
            String manager = "";
            while (true) {
                try {
                    if (!resultSet.next()) break;
                } catch (SQLException e) {
                    System.out.println("this error is in while for get information from database" + " " + e.getMessage());
                }


                try {
                    manager = Integer.toString(resultSet.getInt(1)) + "/" + resultSet.getString(2) + "/" + resultSet.getString(3) + "/" + resultSet.getString(4) + "/" + resultSet.getString(5) + "/" + resultSet.getString(6) + "/" + resultSet.getString(7) + "/" + resultSet.getString(8) + "/" + resultSet.getString(9);
                    //add information from program to array list from Manager class
                    String[] managerProperties = manager.split("/");
                    Manager.managers.add(new Manager((managerProperties[0]), managerProperties[1], managerProperties[2], managerProperties[3], managerProperties[4], managerProperties[5], managerProperties[6], managerProperties[7], managerProperties[8]));

                } catch (SQLException e) {
                    System.out.println("we cannot get information from Manager table" + " " + e.getMessage());
                }
            }


        } else if (object instanceof Employee) {
            Employee.employees.clear();
            ResultSet resultSet = null;
            String sql = "SELECT * FROM Employee";

            try {
                //connect to database and execute query
                Connection connection1 = DriverManager.getConnection(url);
                Statement s = connection1.createStatement();
                resultSet = s.executeQuery(sql);
            } catch (SQLException e) {
                System.out.println("we are in Database class in get information method " + e.getMessage());
            }

            //for save information that insert into employee table
            String employee = "";
            while (true) {
                try {
                    if (!resultSet.next()) break;
                } catch (SQLException e) {
                    System.out.println("this error is in while for get information from database" + " " + e.getMessage());
                }


                try {
                    employee = Integer.toString(resultSet.getInt(1)) + "/" + resultSet.getString(2) + "/" + resultSet.getString(3) + "/" + resultSet.getString(4) + "/" + resultSet.getString(5) + "/" + resultSet.getString(6) + "/" + resultSet.getString(7) + "/" + resultSet.getString(8) + "/" + resultSet.getString(9);
                    //add information from program to array list from Employee class
                    String[] employeeProperties = employee.split("/");
                    Employee.employees.add(new Employee((employeeProperties[0]), employeeProperties[1], employeeProperties[2], employeeProperties[3], employeeProperties[4], employeeProperties[5], employeeProperties[6], employeeProperties[7], employeeProperties[8]));

                } catch (SQLException e) {
                    System.out.println("we cannot get information from Employee table" + " " + e.getMessage());
                }
            }

        } else if (object instanceof Passenger) {
            Passenger.passengers.clear();
            ResultSet resultSet = null;
            String sql = "SELECT * FROM Passenger";

            try {
                //connect to database and execute query
                Connection connection1 = DriverManager.getConnection(url);
                Statement s = connection1.createStatement();
                resultSet = s.executeQuery(sql);
            } catch (SQLException e) {
                System.out.println("we are in Database class in get information method " + e.getMessage());
            }

            //for save information that insert into passenger table
            String passenger = "";
            while (true) {
                try {
                    if (!resultSet.next()) break;
                } catch (SQLException e) {
                    System.out.println("this error is in while for get information from database" + " " + e.getMessage());
                }


                try {
                    passenger = Integer.toString(resultSet.getInt(1)) + "/" + resultSet.getString(2) + "/" + resultSet.getString(3) + "/" + resultSet.getString(4) + "/" + resultSet.getString(5) + "/" + resultSet.getString(6) + "/" + resultSet.getString(7) + "/" + resultSet.getString(8);
                    //add information from program to array list from Passenger class
                    String[] passengerProperties = passenger.split("/");
                    Passenger.passengers.add(new Passenger((passengerProperties[0]), passengerProperties[1], passengerProperties[2], passengerProperties[3], passengerProperties[4], passengerProperties[5], passengerProperties[6], passengerProperties[7]));

                } catch (SQLException e) {
                    System.out.println("we cannot get information from Passenger table" + " " + e.getMessage());
                }
            }
        } else if (object instanceof Airplane) {
            Airplane.airplanes.clear();
            ResultSet resultSet = null;
            String sql = "SELECT * FROM Airplane";

            try {
                //connect to database and execute query
                Connection connection1 = DriverManager.getConnection(url);
                Statement s = connection1.createStatement();
                resultSet = s.executeQuery(sql);
            } catch (SQLException e) {
                System.out.println("we are in Database class in get information method " + e.getMessage());
            }

            //for save information that insert into airplane table
            String airplane = "";
            while (true) {
                try {
                    if (!resultSet.next()) break;
                } catch (SQLException e) {
                    System.out.println("this error is in while for get information from database" + " " + e.getMessage());
                }


                try {
                    airplane = Integer.toString(resultSet.getInt(1)) + "/" + resultSet.getString(2);
                    //add information from program to array list from Airplane class
                    String[] airplanesProperties = airplane.split("/");
                    Airplane.airplanes.add(new Airplane((airplanesProperties[0]), airplanesProperties[1]));

                } catch (SQLException e) {
                    System.out.println("we cannot get information from Passenger table" + " " + e.getMessage());
                }
            }
        } else if (object instanceof Flight) {
            Flight.flights.clear();
            ResultSet resultSet = null;
            String sql = "SELECT * FROM Flight";

            try {
                //connect to database and execute query
                Connection connection1 = DriverManager.getConnection(url);
                Statement s = connection1.createStatement();
                resultSet = s.executeQuery(sql);
            } catch (SQLException e) {
                System.out.println("we are in Database class in get information method " + e.getMessage());
            }

            //for save information that insert into Flight table
            String flight = "";
            while (true) {
                try {
                    if (!resultSet.next()) break;
                } catch (SQLException e) {
                    System.out.println("this error is in while for get information from database" + " " + e.getMessage());
                }


                try {
                    flight = Integer.toString(resultSet.getInt(1)) + "/" + resultSet.getString(2) + "/" + resultSet.getString(3) + "/" + resultSet.getString(4) + "/" + resultSet.getString(5) + "/" + resultSet.getString(6) + "/" + resultSet.getString(7) + "/" + Integer.toString(resultSet.getInt(8));
                    //add information from program to array list from Flight class
                    String[] flightsProperties = flight.split("/");
                    Flight.flights.add(new Flight((flightsProperties[0]), flightsProperties[1], flightsProperties[2], flightsProperties[3], flightsProperties[4], flightsProperties[5], flightsProperties[6], flightsProperties[7]));

                } catch (SQLException e) {
                    System.out.println("we cannot get information from Flight table" + " " + e.getMessage());
                }
            }
        } else if (object instanceof Ticket) {
            Ticket.tickets.clear();
            ResultSet resultSet = null;
            String sql = "SELECT * FROM Ticket";

            try {
                //connect to database and execute query
                Connection connection1 = DriverManager.getConnection(url);
                Statement s = connection1.createStatement();
                resultSet = s.executeQuery(sql);
            } catch (SQLException e) {
                System.out.println("we are in Database class in get information method " + e.getMessage());
            }

            //for save information that insert into ticket table
            String ticket = "";
            while (true) {
                try {
                    if (!resultSet.next()) break;
                } catch (SQLException e) {
                    System.out.println("this error is in while for get information from database" + " " + e.getMessage());
                }


                try {
                    ticket = Integer.toString(resultSet.getInt(1)) + "/" + resultSet.getString(2) + "/" + resultSet.getString(3) + "/" + Integer.parseInt(resultSet.getString(4));
                    //add information from program to array list from Ticket class
                    String[] ticketsProperties = ticket.split("/");
                    Ticket.tickets.add(new Ticket((ticketsProperties[0]), ticketsProperties[1], ticketsProperties[2], ticketsProperties[3]));

                } catch (SQLException e) {
                    System.out.println("we cannot get information from Passenger table" + " " + e.getMessage());
                }
            }
        } else if (object instanceof TicketsAndPassengers) {
            TicketsAndPassengers.ticketsAndPassengers.clear();
            ResultSet resultSet = null;
            String sql = "SELECT * FROM Tickets_And_Passengers";

            try {
                //connect to database and execute query
                Connection connection1 = DriverManager.getConnection(url);
                Statement s = connection1.createStatement();
                resultSet = s.executeQuery(sql);
            } catch (SQLException e) {
                System.out.println("we are in Database class in get information method " + e.getMessage());
            }

            //for save information that insert into tickets and passengers table
            String ticketAndPassenger = "";
            while (true) {
                try {
                    if (!resultSet.next()) break;
                } catch (SQLException e) {
                    System.out.println("this error is in while for get information from database" + " " + e.getMessage());
                }


                try {
                    ticketAndPassenger = resultSet.getInt(1) + "/" + resultSet.getInt(2);
                    //add information from program to array list from TicketsAndPassengers class
                    String[] ticketAndPassengerProperties = ticketAndPassenger.split("/");
                    TicketsAndPassengers.ticketsAndPassengers.add(new TicketsAndPassengers((ticketAndPassengerProperties[0]), ticketAndPassengerProperties[1]));

                } catch (SQLException e) {
                    System.out.println("we cannot get information from Tickets_And_Passengers table" + " " + e.getMessage());
                }
            }
        }
    }


    public static void changePassword(Object object, String username, String pass) {
        if (object instanceof Manager) {
            try {
                String sql = "UPDATE Manager SET Password = ?  WHERE Username = ?";
                //connect to database
                Connection connection1 = DriverManager.getConnection(url);
                PreparedStatement statement = connection1.prepareStatement(sql);
                //update the manager password
                statement.setString(1, pass);
                statement.setString(2, username);
                statement.addBatch();
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println("we are in database class in change password method " + e.getMessage());
            }
        } else if (object instanceof Employee) {
            try {
                String sql = "UPDATE Employee SET Password = ?  WHERE Username = ?";
                //connect to database
                Connection connection1 = DriverManager.getConnection(url);
                PreparedStatement statement = connection1.prepareStatement(sql);
                //update the employee password
                statement.setString(1, pass);
                statement.setString(2, username);
                statement.addBatch();
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println("we are in database class in change password method " + e.getMessage());
            }
        } else if (object instanceof Passenger) {
            try {
                String sql = "UPDATE Passenger SET Password = ?  WHERE Username = ?";
                //connect to database
                Connection connection1 = DriverManager.getConnection(url);
                PreparedStatement statement = connection1.prepareStatement(sql);
                //update the passenger password
                statement.setString(1, pass);
                statement.setString(2, username);
                statement.addBatch();
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println("we are in database class in change password method " + e.getMessage());
            }
        }
    }

}
