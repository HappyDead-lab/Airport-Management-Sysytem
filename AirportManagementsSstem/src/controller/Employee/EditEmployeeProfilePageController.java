package controller.Employee;

import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import model.Employee;

import java.net.URL;
import java.util.ResourceBundle;

public class EditEmployeeProfilePageController implements Initializable {
    @FXML
    public TableView<Employee> employeeTableView;
    private @FXML TableColumn<Employee, String> id;
    private @FXML TableColumn<Employee, String> name;
    private @FXML TableColumn<Employee, String> lastName;
    private @FXML TableColumn<Employee, String> username;
    private @FXML TableColumn<Employee, String> password;
    private @FXML TableColumn<Employee, String> email;
    private @FXML TableColumn<Employee, String> address;
    private @FXML TableColumn<Employee, String> phoneNumber;
    private @FXML TableColumn<Employee, String> salary;
    //save user username
    private String saveUsername;

    public void setUser(String username) {
        this.saveUsername = username;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        id.setCellValueFactory(new PropertyValueFactory<>("iD"));
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        username.setCellValueFactory(new PropertyValueFactory<>("userName"));
        password.setCellValueFactory(new PropertyValueFactory<>("password"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        address.setCellValueFactory(new PropertyValueFactory<>("address"));
        phoneNumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        salary.setCellValueFactory(new PropertyValueFactory<>("salary"));

        //below line is for prevent of getting null variables
        Platform.runLater(() -> {
            if (Employee.employees.size() != 0) {

                for (int i = 0; i < Employee.employees.size(); i++) {
                    if(Employee.employees.get(i).getUserName().equals(saveUsername)) {
                        employeeTableView.getItems().add(Employee.employees.get(i));
                        break;
                    }
                }
            }
        });




        editTableColumn();
    }


    //this method make table view editable
    public void editTableColumn() {

        //make name field editable
        name.setCellFactory(TextFieldTableCell.forTableColumn());
        name.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Employee> selectedItem;
            selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setName(event.getNewValue());
            //change value in array list and database
            updateEmployee(uniqValue, event.getNewValue(), "name");
        });


        //make last name field editable
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Employee> selectedItem;
            selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setLastName(event.getNewValue());
            //change value in array list and database
            updateEmployee(uniqValue, event.getNewValue(), "lastName");
        });

        //make username field editable
        username.setCellFactory(TextFieldTableCell.forTableColumn());
        username.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isUsername = false;
            isUsername = Check.checkUsernameEmployee(event.getNewValue());

            if (isUsername) {

                //get selected row from table view
                ObservableList<Employee> selectedItem;
                selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setUserName(event.getNewValue());
                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "username");
            } else {
                createErrorWindow("This username already exists ");
            }
        });


        //make phone number field editable
        phoneNumber.setCellFactory(TextFieldTableCell.forTableColumn());
        phoneNumber.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPhoneNumber = false;
            isPhoneNumber = Check.checkPhoneNumber(event.getNewValue());

            if (isPhoneNumber) {

                //get selected row from table view
                ObservableList<Employee> selectedItem;
                selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPhoneNumber(event.getNewValue());
                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "phoneNumber");

            } else {
                createErrorWindow("Invalid phone number");
            }
        });

        //make address field editable
        address.setCellFactory(TextFieldTableCell.forTableColumn());
        address.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Employee> selectedItem;
            selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setAddress(event.getNewValue());
            //change value in array list and database
            updateEmployee(uniqValue, event.getNewValue(), "address");
        });

        //make email field editable
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isEmail = false;
            isEmail = Check.checkEmailEmployee(event.getNewValue());
            if (isEmail) {

                //get selected row from table view
                ObservableList<Employee> selectedItem;
                selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setEmail(event.getNewValue());
                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "email");

            } else {
                createErrorWindow("Invalid email");
            }
        });


        employeeTableView.setEditable(true);
    }


    //this method will be always make array list of Employee update
    private void updateEmployee(String username, String newValue, String property) {

        //change old name with new name
        if (property.equals("name")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setName(newValue);
                    break;
                }
            }
        }

        //change old last name with new last name
        if (property.equals("lastName")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setLastName(newValue);
                    break;
                }
            }
        }

        //change old username with new username
        if (property.equals("username")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setUserName(newValue);
                    username = newValue;
                    break;
                }
            }
        }


        //change old phone number with new phone number
        if (property.equals("phoneNumber")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setPhoneNumber(newValue);
                    break;
                }
            }
        }

        //change old address with new address
        if (property.equals("address")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setAddress(newValue);
                    break;
                }
            }
        }

        //change old email with new email
        if (property.equals("email")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setEmail(newValue);
                    break;
                }
            }
        }
        //update Employee table in database
        for (int i = 0; i < Employee.employees.size(); i++) {
            Database.updateInformation(Employee.employees.get(i),Employee.employees.get(i).getID());
        }
    }

    private void createErrorWindow(String message) {
        ErrorPageController.text = message;
        ErrorPageController.createErrorWindow();
    }
}
