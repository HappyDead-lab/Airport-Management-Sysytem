package controller.Employee;

import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import controller.MessagePageController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import model.Employee;


import java.io.IOException;

public class EmployeePageController {

    private @FXML Button editProfileBTN;
    private @FXML Button changePassBTN;
    private @FXML Button changeBTN;
    private @FXML PasswordField passwordPSF;
    private @FXML PasswordField confirmationPSF;
    private @FXML Label successfulLBL;
    //for make pages movable
    private double xOffset = 0;
    private double yOffset = 0;

    //this boolean is for personal profile management button
    private boolean checkPersonalBTN = false;

    //this boolean is for change password button
    private boolean checkChangeBTN = false;

    //this variable for save input username that manager enter with it
    private  String saveUsername;

    public  void setUsername(String username) {
        saveUsername = username;
    }

    //set on action for personal profile management button
    public void setOnActionPersonalBTN(ActionEvent event){
        if(!checkPersonalBTN) {
            //for first click
            visibleBTN(editProfileBTN,0.50);
            visibleBTN(changePassBTN,0.75);
            checkPersonalBTN = true;
        }else if (checkPersonalBTN){
            //for second click
            invisibleLBL(successfulLBL,0.50);
            invisibleBTN(changePassBTN,1.50);
            invisibleBTN(changeBTN,0.95);
            invisiblePSF(confirmationPSF,1.25);
            invisiblePSF(passwordPSF,1.40);
            invisibleBTN(editProfileBTN,1.60);
            checkPersonalBTN = false;
        }

    }

    //set on action for change password button
    public void setOnActionChangePassBTN(ActionEvent event) {
        if (!checkChangeBTN) {
            //for first click
            visiblePSF(passwordPSF,0.75);
            visiblePSF(confirmationPSF,0.95);
            visibleBTN(changeBTN,1.25);
            checkChangeBTN = true;
        }else if(checkChangeBTN){
            //for second click
            invisibleLBL(successfulLBL,0.50);
            invisibleBTN(changeBTN,0.95);
            invisiblePSF(confirmationPSF,1.25);
            invisiblePSF(passwordPSF,1.40);
            checkChangeBTN = false;
        }
    }

    //set on action for change button
    public void setOnActionChangeBTN(ActionEvent event) {
        Stage stage1;
        //show appropriate error
        if (passwordPSF.getText().equals("") || confirmationPSF.getText().equals("")) {
            //if at least one of field is empty

            //set message of error
            ErrorPageController.text = "Please fill all of the fields";
            ErrorPageController.createErrorWindow();
        } else if (!(passwordPSF.getText().equals(confirmationPSF.getText()))) {
            //if new password is not equal to its confirmation

            //set message of error
            ErrorPageController.text = "Please enter correct confirmation";
            ErrorPageController.createErrorWindow();
        }else{
            //change password of manager

            //this variable check input password is really a password or not
            boolean isPass = false;

            isPass = Check.checkPassword(passwordPSF.getText());
            if(isPass) {
                //here we find the user that enter an change password
                for (int i = 0; i < Employee.employees.size(); i++) {
                    if (Employee.employees.get(i).getUserName().equals(saveUsername)) {
                        Employee.employees.get(i).setPassword(passwordPSF.getText());
                        Database.changePassword(new Employee(), saveUsername, passwordPSF.getText());
                        break;
                    }
                }

                //clear password fields
                passwordPSF.clear();
                confirmationPSF.clear();


                visibleLBL(successfulLBL, 0.1);
            }else {
                //set error message
                ErrorPageController.text = "It is less than 5 characters";
                ErrorPageController.createErrorWindow();
            }
        }
    }

    //set on action for information flight button
    public void setOnActionInformationFlightBTN(ActionEvent event){
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader  = new FXMLLoader(getClass().getResource("/view/Flight/FlightInformationPage.fxml"));
        BorderPane root = null;
        try {
            //load page
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //get mouse position
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set mouse position
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });
        //set scene
        stage.setScene(new Scene(root));
        stage.show();
    }

    //set on action for send message button
    public void setOnActionSendMessageBTN(ActionEvent event){
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MessagePage.fxml"));
        AnchorPane root = null;
        try {
            //load page
            root = (AnchorPane) loader.load();
            //get controller
            MessagePageController controller = loader.getController();
            //determine user is employee
            controller.setEmployee(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(new Scene(root));
        stage.show();
    }
    //set on action for edit profile button
    public void setOnActionEditProfileBTN(ActionEvent event){
        //create stage an set style
        Stage stage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Employee/EditEmployeeProfilePage.fxml"));
        //create root
        BorderPane root = null;
        //load page
        try {
            root = loader.load();
            //get controller
            EditEmployeeProfilePageController controller = loader.getController();
            //pass user username
            controller.setUser(saveUsername);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //get mouse position
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set mouse position
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });

        //set scene
        stage.setScene(new Scene(root));
        stage.show();
    }

    //this method visible a button
    private void visibleBTN(Button button , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                button.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //this method invisible a button
    private void invisibleBTN(Button button , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it invisible after a few second
                                button.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

    //this method visible a label
    private void visibleLBL(Label label , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //this method visible a label
    private void invisibleLBL(Label label , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it invisible after a few second
                                label.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

    //this method visible a password field
    private void visiblePSF(PasswordField passwordField , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                passwordField.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

    //this method invisible a password field
    private void invisiblePSF(PasswordField passwordField , double time){

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(time),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it invisible after a few second
                                passwordField.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }
}
