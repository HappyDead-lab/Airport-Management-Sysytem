package controller.Employee;

import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Employee;
import java.net.URL;
import java.util.ResourceBundle;

public class AddEmployeePageController implements Initializable {

    @FXML private TextField firstNameTXF;
    @FXML private TextField lastNameTXF;
    @FXML private TextField phoneNumberTXF;
    @FXML private TextField addressTXF;
    @FXML private TextField usernameTXF;
    @FXML private TextField emailTXF;
    @FXML private TextField salaryTXF;
    @FXML private PasswordField passwordPSF;
    @FXML private PasswordField confirmationPSF;
    @FXML private Label invalidPhoneNumberLBL;
    @FXML private Label invalidEmailLBL;
    @FXML private Label invalidUsername;
    @FXML private Label invalidSalaryLBL;
    @FXML private Label invalidPassLBL;
    @FXML private Button hireBTN;
    private TableView<Employee> tableView;

    public void setTableView(TableView<Employee> tableView) {
        this.tableView = tableView;
    }

    public void setOnActionHireBTN(ActionEvent event)  {
        //get current stage
        Stage stage = (Stage)hireBTN.getScene().getWindow();

        if(firstNameTXF.getText().equals("") || lastNameTXF.getText().equals("") || phoneNumberTXF.getText().equals("") || addressTXF.getText().equals("") || usernameTXF.getText().equals("") || emailTXF.getText().equals("") || salaryTXF.getText().equals("") || passwordPSF.getText().equals("") || confirmationPSF.getText().equals("")){
            //show error if one of the fields be empty
            ErrorPageController.text = "Please fill all parts of the fields";
            ErrorPageController.createErrorWindow();
        }else if(!(passwordPSF.getText().equals(confirmationPSF.getText()))){
            ErrorPageController.text = "Please enter correct confirmation";
            ErrorPageController.createErrorWindow();
        }else{
            //to check that some input value is correct or not
            boolean isPhoneNumber = false;
            boolean isUsername = false;
            boolean isEmail = false;
            boolean isSalary = false;
            boolean isPassword = false;

            Employee employee = new Employee();

            //check input salary is valid or not
            isSalary = Check.checkSalaryEmployee(salaryTXF.getText());
            if(isSalary){
                invisible(invalidSalaryLBL);
                employee.setSalary(salaryTXF.getText());
            }else {
                //visible error label
                visible(invalidSalaryLBL);
            }

            //check input email is valid or not
            isEmail = Check.checkEmailEmployee(emailTXF.getText());
            if(isEmail){
                invisible(invalidEmailLBL);
                employee.setEmail(emailTXF.getText());
            }else {
                //visible error label
                visible(invalidEmailLBL);
            }

            //check input username is valid or not
            isUsername = Check.checkUsernameEmployee(usernameTXF.getText());
            if(isUsername){
                invisible(invalidUsername);
                employee.setUserName(usernameTXF.getText());
            }else{
                //visible error label
                visible(invalidUsername);
            }


            //check input phone number is valid or not
            isPhoneNumber = Check.checkPhoneNumber(phoneNumberTXF.getText());
            if(isPhoneNumber) {
                invisible(invalidPhoneNumberLBL);
                employee.setPhoneNumber(phoneNumberTXF.getText());
            }else{
                //visible error label
                visible(invalidPhoneNumberLBL);
            }

            isPassword = Check.checkPassword(passwordPSF.getText());
            if(isPassword){
                invisible(invalidPassLBL);
                employee.setPassword(passwordPSF.getText());
            }else{
                //visible error label
                visible(invalidPassLBL);
            }

            if(isEmail && isPhoneNumber && isUsername && isSalary && isPassword) {
                //invisible all labels
                invisible(invalidPassLBL);
                invisible(invalidPhoneNumberLBL);
                invisible(invalidUsername);
                invisible(invalidEmailLBL);
                invisible(invalidSalaryLBL);

                //add remains information
                employee.setName(firstNameTXF.getText());
                employee.setLastName(lastNameTXF.getText());
                employee.setPassword(passwordPSF.getText());
                employee.setAddress(addressTXF.getText());
                employee.setSalary(salaryTXF.getText());

                //save information on database
                Database.setInformation(employee);
                //save information in array list from database
                Database.getInformation(new Employee());
                employee.setID(Employee.employees.get(Employee.employees.size() - 1).getID());
                //add information to table view at the moment
                tableView.getItems().add(employee);
                stage.close();
            }
        }
    }


    //make a label visible
    private void visible(Label label){
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //make a label invisible
    private void invisible(Label label){
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
