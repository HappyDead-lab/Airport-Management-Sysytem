package controller.Employee;

import Main.Main;
import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Employee;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class EmployeeInformationPageController implements Initializable {
    private @FXML
    TableColumn<Employee, String> id;
    private @FXML
    TableColumn<Employee, String> name;
    private @FXML
    TableColumn<Employee, String> lastName;
    private @FXML
    TableColumn<Employee, String> username;
    private @FXML
    TableColumn<Employee, String> password;
    private @FXML
    TableColumn<Employee, String> email;
    private @FXML
    TableColumn<Employee, String> address;
    private @FXML
    TableColumn<Employee, String> phoneNumber;
    private @FXML
    TableColumn<Employee, String> salary;
    @FXML
    private TableView<Employee> employeeTableView;
    @FXML
    private Button hireBTN;
    @FXML
    private Button exitBTN;
    //this variables help that make window movable
    private double xOffset = 0;
    private double yOffset = 0;

    //set on action for exit button
    public void setOnActionExitBTN(ActionEvent event) {
        Stage stage = (Stage) exitBTN.getScene().getWindow();
        stage.close();
    }


    //set on action for hire button
    public void setOnActionHireBTN(ActionEvent event) {
        Stage stage = (Stage) hireBTN.getScene().getWindow();
        stage = new Stage(StageStyle.UNDECORATED);
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/view/Employee/AddEmployeePage.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        AddEmployeePageController controller = loader.getController();
        controller.setTableView(employeeTableView);

        AnchorPane root = loader.getRoot();

        //get position of mouse
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        //set position of mouse
        Stage finalStage = stage;
        root.setOnMouseDragged(event1 -> {
            finalStage.setX(event1.getScreenX() - xOffset);
            finalStage.setY(event1.getScreenY() - yOffset);
        });


        stage.setScene(new Scene(root));
        stage.show();

    }

    //set on action for remove button
    public void setOnActionFireBTN(ActionEvent event) {
        //get all information from table view
        ObservableList<Employee> allEmployee , selectionEmployee = null;
        allEmployee = employeeTableView.getItems();
        try {
            //get selection row
            selectionEmployee = employeeTableView.getSelectionModel().getSelectedItems();
        }catch (NullPointerException e){
            System.out.println("Please select a row " + e.getMessage());
        }



        //remove the employee in array list and database
        for (int i = 0; i < selectionEmployee.size(); i++) {
            for (int j = 0; j < Employee.employees.size(); j++) {
                if (selectionEmployee.get(i).getUserName().equals(Employee.employees.get(j).getUserName())) {
                    //found it
                    Database.removeInformation(new Employee(), Employee.employees.get(j).getUserName());
                    Employee.employees.remove(j);
                    break;
                }
            }
        }
        //remove it from table view
        selectionEmployee.forEach(allEmployee::remove);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if (Employee.employees.size() != 0) {

            id.setCellValueFactory(new PropertyValueFactory<>("iD"));
            name.setCellValueFactory(new PropertyValueFactory<>("name"));
            lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
            username.setCellValueFactory(new PropertyValueFactory<>("userName"));
            password.setCellValueFactory(new PropertyValueFactory<>("password"));
            email.setCellValueFactory(new PropertyValueFactory<>("email"));
            address.setCellValueFactory(new PropertyValueFactory<>("address"));
            phoneNumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
            salary.setCellValueFactory(new PropertyValueFactory<>("salary"));
            for (int i = 0; i < Employee.employees.size(); i++) {
                employeeTableView.getItems().add(Employee.employees.get(i));
            }
        }
        editTableColumn();
    }


    //this method make table view editable
    public void editTableColumn() {

        //make name field editable
        name.setCellFactory(TextFieldTableCell.forTableColumn());
        name.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Employee> selectedItem;
            selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setName(event.getNewValue());
            //change value in array list and database
            updateEmployee(uniqValue, event.getNewValue(), "name");
        });


        //make last name field editable
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Employee> selectedItem;
            selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setLastName(event.getNewValue());
            //change value in array list and database
            updateEmployee(uniqValue, event.getNewValue(), "lastName");

        });

        //make username field editable
        username.setCellFactory(TextFieldTableCell.forTableColumn());
        username.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isUsername = false;
            isUsername = Check.checkUsernameEmployee(event.getNewValue());

            if (isUsername) {

                //get selected row from table view
                ObservableList<Employee> selectedItem;
                selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setUserName(event.getNewValue());

                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "username");
            } else {
                createErrorWindow("This username already exists ");
            }
        });

        //make password field editable
        password.setCellFactory(TextFieldTableCell.forTableColumn());
        password.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPassword = false;
            isPassword = Check.checkPassword(event.getNewValue());
            if (isPassword) {


                //get selected row from table view
                ObservableList<Employee> selectedItem;
                selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();


                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPassword(event.getNewValue());


                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "password");

            } else {
                createErrorWindow("It is less than 5 character");
            }
        });

        //make phone number field editable
        phoneNumber.setCellFactory(TextFieldTableCell.forTableColumn());
        phoneNumber.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isPhoneNumber = false;
            isPhoneNumber = Check.checkPhoneNumber(event.getNewValue());

            if (isPhoneNumber) {

                //get selected row from table view
                ObservableList<Employee> selectedItem;
                selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setPhoneNumber(event.getNewValue());
                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "phoneNumber");

            } else {
                createErrorWindow("Invalid phone number");
            }
        });

        //make address field editable
        address.setCellFactory(TextFieldTableCell.forTableColumn());
        address.setOnEditCommit(event -> {
            //get selected row from table view
            ObservableList<Employee> selectedItem;
            selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
            //save a uniq filed of selection user
            String uniqValue = selectedItem.get(0).getUserName();
            //update values in table view
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setAddress(event.getNewValue());
            //change value in array list and database
            updateEmployee(uniqValue, event.getNewValue(), "address");
        });

        //make email field editable
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isEmail = false;
            isEmail = Check.checkEmailEmployee(event.getNewValue());
            if (isEmail) {

                //get selected row from table view
                ObservableList<Employee> selectedItem;
                selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setEmail(event.getNewValue());
                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "email");

            } else {
                createErrorWindow("Invalid email");
            }
        });

        //make salary field editable
        salary.setCellFactory(TextFieldTableCell.forTableColumn());
        salary.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isSalary = false;
            isSalary = Check.checkSalaryEmployee(event.getNewValue());
            if (isSalary) {

                //get selected row from table view
                ObservableList<Employee> selectedItem;
                selectedItem = employeeTableView.getSelectionModel().getSelectedItems();
                //save a uniq filed of selection user
                String uniqValue = selectedItem.get(0).getUserName();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setSalary(event.getNewValue());

                //change value in array list and database
                updateEmployee(uniqValue, event.getNewValue(), "salary");

            } else {
                createErrorWindow("Please enter number");
            }
        });


        employeeTableView.setEditable(true);
    }


    //this method will be always make array list of Employee update
    private void updateEmployee(String username, String newValue, String property) {

        //change old name with new name
        if (property.equals("name")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setName(newValue);
                }
            }
        }

        //change old last name with new last name
        if (property.equals("lastName")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setLastName(newValue);
                }
            }
        }

        //change old username with new username
        if (property.equals("username")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setUserName(newValue);
                }
            }
        }

        //change old password with new password
        if (property.equals("password")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setPassword(newValue);
                }
            }
        }

        //change old phone number with new phone number
        if (property.equals("phoneNumber")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setPhoneNumber(newValue);
                }
            }
        }

        //change old address with new address
        if (property.equals("address")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setAddress(newValue);
                }
            }
        }

        //change old email with new email
        if (property.equals("email")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setEmail(newValue);
                }
            }
        }

        //change old salary with new salary
        if (property.equals("salary")) {
            for (int i = 0; i < Employee.employees.size(); i++) {
                if (Employee.employees.get(i).getUserName().equals(username)) {
                    Employee.employees.get(i).setSalary(newValue);
                }
            }
        }

        //update Employee table in database
        for (int i = 0; i < Employee.employees.size(); i++) {
            Database.updateInformation(Employee.employees.get(i),Employee.employees.get(i).getID());
        }
    }

    private void createErrorWindow(String message) {
        ErrorPageController.text = message;
        ErrorPageController.createErrorWindow();
    }
}
