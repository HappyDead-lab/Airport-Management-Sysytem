package controller;
import model.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;

//this class built for check some filed that require to check
public class Check {

    //this method check that salary is valid or not (Employee)
    public static boolean checkSalaryEmployee(String salary){
        return salary.matches("[0-9]+");
    }

    //this method check email is valid or not (Employee)
    public static boolean checkEmailEmployee(String email){
        boolean check = true;
        for (int i = 0; i < Employee.employees.size(); i++) {
            if(Employee.employees.get(i).getEmail().equals(email)){
                check = false;
            }
        }
        String regex = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
        return check && email.matches(regex);
    }



    //this method check that username is valid or not (Employee)
    public static boolean checkUsernameEmployee(String username) {
        for(int i = 0 ; i < Employee.employees.size() ; i++){
            if(Employee.employees.get(i).getUserName().equals(username))
                return false;
        }
        return true;
    }
    //this method check phone number is valid or not
    public static boolean checkPhoneNumber(String phoneNumber){

        return ((phoneNumber.length() == 11) && (phoneNumber.matches("[0-9]+")));
    }
    //this method is check is valid or not
    public static boolean checkPassword(String pass){
        return pass.length() >= 5;
    }


    //this method check that username is valid or not (Manager)
    public static boolean checkUsernameManager(String username) {
        for(int i = 0 ; i < Manager.managers.size() ; i++){
            if(Manager.managers.get(i).getUserName().equals(username))
                return false;
        }
        return true;
    }


    //this method check email is valid or not (Manager)
    public static boolean checkEmailManager(String email){
        boolean check = true;
        for (int i = 0; i < Manager.managers.size(); i++) {
            if(Manager.managers.get(i).getEmail().equals(email)){
                check = false;
            }
        }
        String regex = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
        return check && email.matches(regex);
    }


    //this method check email is valid or not (Passenger)
    public static boolean checkEmailPassenger(String email){
        boolean check = true;
        for (int i = 0; i < Passenger.passengers.size(); i++) {
            if(Passenger.passengers.get(i).getEmail().equals(email)){
                check = false;
            }
        }
        String regex = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
        return check && email.matches(regex);
    }

    //this method check that username is valid or not (Passenger)
    public static boolean checkUsernamePassenger(String username) {
        for(int i = 0 ; i < Passenger.passengers.size() ; i++){
            if(Passenger.passengers.get(i).getUserName().equals(username))
                return false;
        }
        return true;
    }



    //this method check that number of seats is valid or not(Airplane)
    public static boolean checkNumber(String number){
        return number.matches("[0-9]+");
    }

    //this method check that date input is valid or not(Flight)
    public static boolean checkDate(String date){
        try {
            LocalDate tempDate = LocalDate.parse(date);
            return true;
        }catch (DateTimeParseException | NullPointerException e){
            System.out.println("Invalid date string: " + date);
        }
        return false;

    }

    //this method check that time input is valid or not(Flight)
    public static boolean checkTime(String time) {

        try {
            LocalTime tempTime = LocalTime.parse(time);
            return true;
        }catch (DateTimeParseException | NullPointerException e){
            System.out.println("Invalid time string: " + time);
        }

        return false;
    }

    //this method check that airplane id have been existed or have not
    public static boolean checkAirplaneID(String airplaneID){
        for (int i = 0; i < Airplane.airplanes.size(); i++) {
            if(Airplane.airplanes.get(i).getID().equals(airplaneID)){
                return true;
            }
        }
        return false;
    }

    //this method check that flight id have been existed or have not
    public static boolean checkFlightID(String flightID){
        for (int i = 0; i < Flight.flights.size(); i++) {
            if(Flight.flights.get(i).getID().equals(flightID)){
                return true;
            }
        }
        return false;
    }
    //this method check that ticket was bought is already exist or does not
    public static boolean checkTicket(String ticketID , String passengerID){
        for (int i = 0; i < TicketsAndPassengers.ticketsAndPassengers.size(); i++) {
            if(TicketsAndPassengers.ticketsAndPassengers.get(i).getTicketID().equals(ticketID) &&TicketsAndPassengers.ticketsAndPassengers.get(i).getPassengerID().equals(passengerID) ){
                return false;
            }

        }
        return true;
    }
}
