package controller.Airplane;

import Main.Main;
import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import controller.Flight.FlightInformationPageController;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AirplaneInformationPageController implements Initializable {
    private @FXML Button exitBTN;
    private @FXML Button showBTN;
    private @FXML TableView<Airplane> airplaneTableView;
    private @FXML TableColumn<Airplane, String> numberOfSeats;
    private @FXML TableColumn<Manager, String> id;
    //for make pages movable

    private double xOffset = 0;
    private double yOffset = 0;


    //set on action for add button
    public void setOnActionAddBTN(ActionEvent event) {
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);

        //load page
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/view/Airplane/AddAirplanePage.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //create root
        AnchorPane root = loader.getRoot();
        //get controller
        AddAirplanePageController controller = loader.getController();
        controller.setTableView(airplaneTableView);

        //get mouse position
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set mouse position
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });

        //set scene
        stage.setScene(new Scene(root));
        stage.show();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        id.setCellValueFactory(new PropertyValueFactory<>("iD"));
        numberOfSeats.setCellValueFactory(new PropertyValueFactory<>("numberOfSeats"));

        for (int i = 0; i < Airplane.airplanes.size(); i++) {
            airplaneTableView.getItems().add(Airplane.airplanes.get(i));
        }
        editTableColumn();
    }

    private void editTableColumn() {
        //make salary field editable
        numberOfSeats.setCellFactory(TextFieldTableCell.forTableColumn());
        numberOfSeats.setOnEditCommit(event -> {
            //to check that input value is correct or not
            boolean isNumberOfSeats = false;
            isNumberOfSeats = Check.checkNumber(event.getNewValue());

            if (isNumberOfSeats) {

                //get selected row from table view
                ObservableList<Airplane> selectedItem;
                selectedItem = airplaneTableView.getSelectionModel().getSelectedItems();
                //save old value in another variable
                String save = selectedItem.get(0).getID();
                //update values in table view
                event.getTableView().getItems().get(event.getTablePosition().getRow()).setNumberOfSeats(event.getNewValue());

                //change value in array list
                updateAirplane(save, event.getNewValue());
                //update database
               // Database.setInformation(new Airplane());

            } else {
                ErrorPageController.text = "Please enter number";
                ErrorPageController.createErrorWindow();
            }
        });
        airplaneTableView.setEditable(true);


    }

    private void updateAirplane(String id, String newValue) {

        for (int i = 0; i < Airplane.airplanes.size(); i++) {
            if (Airplane.airplanes.get(i).getID().equals(id)) {
                Airplane.airplanes.get(i).setNumberOfSeats(newValue);
                break;
            }
        }
        //update airplane
        for (int i = 0; i < Airplane.airplanes.size(); i++) {
            Database.updateInformation(Airplane.airplanes.get(i),Airplane.airplanes.get(i).getID());
        }
    }





    //set on action for remove button
    public void setOnActionRemoveBTN(ActionEvent event) {
        //get all information from table view
        ObservableList<Airplane> allAirplanes , selectionAirplane = null;
        allAirplanes = airplaneTableView.getItems();
        try {
            //get selected row
            selectionAirplane = airplaneTableView.getSelectionModel().getSelectedItems();
        }catch (NullPointerException e){
            System.out.println("Please select a row " + e.getMessage());
        }

        //remove all passengers that related with a flightID tha related to tha airplaneID
        for (int i = 0; i < Airplane.airplanes.size(); i++) {
            for (int j = 0; j < Flight.flights.size(); j++) {
                if(Airplane.airplanes.get(i).getID().equals(Flight.flights.get(j).getAirplaneID())){
                    for (int k = 0; k < Ticket.tickets.size(); k++) {
                        if (Ticket.tickets.get(k).getFlightID().equals(Flight.flights.get(j).getID())) {
                            for (int l = 0; l < Ticket.tickets.get(k).getPassengers().size(); l++) {
                                Database.removeInformation(new Passenger(),Ticket.tickets.get(k).getPassengers().get(l).getUserName());
                            }
                        }
                    }
                }
            }
        }


        //find airplane to remove it in array list
        for (int i = 0; i < selectionAirplane.size(); i++) {
            for (int j = 0; j < Airplane.airplanes.size(); j++) {
                if (selectionAirplane.get(i).getID().equals(Airplane.airplanes.get(j).getID())) {
                    Database.removeInformation(new Airplane(), Airplane.airplanes.get(j).getID());
                    Airplane.airplanes.remove(j);
                    break;
                }
            }
        }
        //update the related array list
        Database.getInformation(new Flight());
        Database.getInformation(new Ticket());
        Database.getInformation(new Passenger());
        //update lists
        Main.setPassengers();
        Main.setTicketsAndPassengers();
        Main.setTickets();
        Main.setFlights();
        //remove the row from table view
        selectionAirplane.forEach(allAirplanes::remove);
    }

    //set on action for exit button
    public void setOnActionExitBTN(ActionEvent event){
        Stage stage = (Stage)exitBTN.getScene().getWindow();
        stage.close();
    }

    //set on action for show button
    public void setOnActionShowBTN(ActionEvent event){
        //save information of selected airplane(selected row actually)
        ObservableList<Airplane> selectedItem;
        selectedItem = airplaneTableView.getSelectionModel().getSelectedItems();

        Stage stage = (Stage)showBTN.getScene().getWindow();
        stage = new Stage(StageStyle.DECORATED);
        //load page
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Flight/FlightInformationPage.fxml"));
        BorderPane root = null;
        try {
           root = (BorderPane) loader.load();
            FlightInformationPageController controller = loader.<FlightInformationPageController>getController();
            controller.setCheck(true);
            controller.setSaveAirplaneID(selectedItem.get(0).getID());

        } catch (NullPointerException e){
            System.out.println(e.getMessage());
        }catch (IOException e) {
            e.printStackTrace();
        }




        //make next page movable
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        Stage finalStage = stage;
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                finalStage.setX(event.getScreenX() - xOffset);
                finalStage.setY(event.getScreenY() - yOffset);
            }
        });

        stage.setScene(new Scene(root));
        stage.show();
    }
}
