package controller.Airplane;

import controller.Check;
import controller.Database;
import controller.ErrorPageController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import model.Airplane;

import java.io.IOException;

public class AddAirplanePageController {
    @FXML private Button cancelBTN;
    @FXML private Button addBTN;
    @FXML private TextField numberOfSeatsTXF;
    @FXML private Label invalidNumberOfSeatsLBL;
    private TableView<Airplane> tableView;

    public void setTableView(TableView<Airplane> tableView) {
        this.tableView = tableView;
    }

    //set on action for cancel button
    public void setOnActionCancelBTN(ActionEvent event){
        Stage stage = (Stage)cancelBTN.getScene().getWindow();
        stage.close();
    }

    //set on action for add button
    public void setOnActionAddBTN(ActionEvent event){
        //get current window
        Stage stage = (Stage)addBTN.getScene().getWindow();

            if(numberOfSeatsTXF.getText().equals("")){
                //show error if one the field was empty
                ErrorPageController.text = "Please fill field";
                ErrorPageController.createErrorWindow();

            }else{
                //this variable check is number of seats is valid or not
                boolean isNumberOfSeats = false;
                isNumberOfSeats = Check.checkNumber(numberOfSeatsTXF.getText());
                if(isNumberOfSeats){
                    invisible(invalidNumberOfSeatsLBL);
                    //add information to an object
                    Airplane airplane = new Airplane();
                    airplane.setNumberOfSeats(numberOfSeatsTXF.getText());
                    //add information to database

                    Database.setInformation(airplane);
                    //save information in array list from database
                    Database.getInformation(new Airplane());
                    airplane.setID(Airplane.airplanes.get(Airplane.airplanes.size() - 1).getID());

                    tableView.getItems().add(airplane);
                    stage.close();

                }else {
                    visible(invalidNumberOfSeatsLBL);
                }
            }
    }


    //make a label visible
    private void visible(Label label){
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(true);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }


    //make a label invisible
    private void invisible(Label label){
        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                //make it visible after a few second
                                label.setVisible(false);
                            }
                        }));
        animation.setCycleCount(1);
        animation.play();
    }

}
