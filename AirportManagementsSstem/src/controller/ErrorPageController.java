package controller;

import Main.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ErrorPageController implements Initializable {
    private @FXML Button okBTN;
    private @FXML Label errorLBL;
    //this variable checks that error window just opens once
    private static boolean isOpen = false;

    public static String text = "";



    private void setErrorLBL(){
        errorLBL.setText(this.text);
    }

    public void setOkBTN(ActionEvent event) {
        //get current window
        Stage stage = (Stage)okBTN.getScene().getWindow();
        stage.close();
        isOpen = false;
    }

    public static void createErrorWindow() {
        if (!isOpen) {
            //create stage and set style
            Stage stage1 = new Stage(StageStyle.UNDECORATED);
            ;

            AnchorPane root1 = null;

            //load error page
            try {
                root1 = FXMLLoader.load(Main.class.getResource("/view/ErrorPage.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            //set style
            stage1.initStyle(StageStyle.TRANSPARENT);
            //create scene
            Scene scene = new Scene(root1);
            //set style
            scene.setFill(Color.TRANSPARENT);

            stage1.setScene(scene);

            stage1.show();

            isOpen = true;

        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setErrorLBL();
    }
}
