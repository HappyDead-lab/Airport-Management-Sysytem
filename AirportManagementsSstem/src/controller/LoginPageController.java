package controller;

import Main.Main;
import controller.Employee.EmployeePageController;
import controller.Manager.ManagerPageController;
import controller.Passenger.PassengerPageController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Employee;
import model.Manager;
import model.Passenger;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginPageController implements Initializable {

    public LoginPageController() {

    }
    //password of super admin
    public static String passWord = "123456";
    //this variable make pages movable
    private double xOffset = 0;
    private double yOffset = 0;

    private @FXML Button exitBTN;
    private @FXML Button enterBTN;
    private @FXML TextField userNameTXF;
    private @FXML TextField passwordTXF;

    //this method is for write new password in file
    public static void write() throws IOException {
        FileWriter writer = new FileWriter("password.txt");
        BufferedWriter buffered = new BufferedWriter(writer);
        buffered.write(passWord);
        buffered.close();
    }
    //this method is for read the new password from file
    public static void read() throws IOException{
        FileReader reader = new FileReader("password.txt");
        BufferedReader buffered = new BufferedReader(reader);
        String temp = "";
        while ((temp = buffered.readLine()) != null){
            passWord = temp;
        }
        buffered.close();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


    //set on action for exit button
    public void setActionExitBTN(ActionEvent event) {
        //get current Stage
        Stage stage = (Stage) exitBTN.getScene().getWindow();
        //close the current window
        stage.close();
    }

    //set on action for forget password button
    public void setOnActionForgetPasswordBTN(ActionEvent event)  {
        //create stage and set style
        Stage stage = new Stage(StageStyle.UNDECORATED);
        //create root and load require page
        AnchorPane root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/ForgetPassword.fxml"));
        } catch (IOException e) {
            System.out.println("we are in login page controller class in set on action for forget password btn method" + e.getMessage());
        }
        //set style
        stage.initStyle(StageStyle.TRANSPARENT);
        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        //set scene
        stage.setScene(scene);
        stage.show();
    }

    //set on action for create passenger button
    public void setOnActionCreatePassengerBTN(ActionEvent event){
        //create stage and set style
        Stage stage = new Stage(StageStyle.DECORATED);
        //create root and load require page
        AnchorPane root = null;
        try {
             root = FXMLLoader.load(getClass().getResource("/view/Passenger/AddPassengerPage.fxml"));
        } catch (IOException e) {
            System.out.println("we are in login page controller class in set on action for create passenger btn method" + e.getMessage());
        }

        //get position of mouse
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set position of mouse
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });

        //set scene
        stage.setScene(new Scene(root));
        stage.show();
    }

    //this method is for set on action for enter button
   public void setOnActionEnterBTN(ActionEvent event)  {
        boolean isManager = false;
        boolean isEmployee = false;
        boolean isPassenger = false;
        boolean isSuperAdmin = false;
        int storedIndex = 0;
        //get current stage
        Stage stage = (Stage) enterBTN.getScene().getWindow();
        
        //these statements is for check password and username of admin and show appropriate message if one of them is wrong
        if(userNameTXF.getText().equals("admin")){
            isSuperAdmin = true;
            if(passwordTXF.getText().equals(passWord)){
                createUsersPage(stage , "/view/SuperAmin/SuperAdminPage.fxml");
            }else{
                showError("Please enter correct password");
            }
        }

        //here we check the username will be a manager or won't
        isManager = checkManager(userNameTXF.getText());
        if (isManager){
            //save index of input username
            storedIndex = indexManager(userNameTXF.getText());
            if(passwordTXF.getText().equals(Manager.managers.get(storedIndex).getPassword())){
               createUsersPage(stage, "/view/Manager/ManagerPage.fxml",userNameTXF.getText(),"manager");
            }else if(!isSuperAdmin){
                showError("Please enter correct password");
            }
        }

       //here we check the username will be a employee or won't
       isEmployee = checkEmployee(userNameTXF.getText());
       if (isEmployee){
           //save index of input username
           storedIndex = indexEmployee(userNameTXF.getText());
           if(passwordTXF.getText().equals(Employee.employees.get(storedIndex).getPassword())){
               createUsersPage(stage, "/view/Employee/EmployeePage.fxml",userNameTXF.getText(),"employee");
           }else if (!(isSuperAdmin || isManager)){
               showError("Please enter correct password");
           }
       }

       //here we check the username will be a passenger or won't
       isPassenger = checkPassenger(userNameTXF.getText());
       if (isPassenger){
           //save index of input username
           storedIndex = indexPassenger(userNameTXF.getText());
           if(passwordTXF.getText().equals(Passenger.passengers.get(storedIndex).getPassword())){
               createUsersPage(stage, "/view/Passenger/PassengerPage.fxml",userNameTXF.getText(),"passenger");
           }else if (!(isSuperAdmin || isManager || isEmployee)){
               showError("Please enter correct password");
           }
       }



        //if username text field was empty or username does not exist
       if(!(isEmployee || isManager || isPassenger || isSuperAdmin) || userNameTXF.getText().equals("")){
           showError("Please enter correct username");
       }
   }

    //this method return index of manager that username belongs to him\her
    private int indexPassenger(String username) {
        int index = 0;
        for (int i = 0; i < Passenger.passengers.size(); i++) {
            if(Passenger.passengers.get(i).getUserName().equals(username)){
                //find index
                index = i;
                break;

            }
        }
        return index;
    }

    //this method check input username belongs to passenger or do not
    private boolean checkPassenger(String username) {
        for (int i = 0; i < Passenger.passengers.size(); i++) {
            if(Passenger.passengers.get(i).getUserName().equals(username)){
                //find username
                return true;
            }
        }
        return false;
    }




    //this method show error window if password is not equal to input password
    private void showError(String message) {
        //set the error message
        ErrorPageController.text = message;
        ErrorPageController.createErrorWindow();
    }

    //this method load a page
    //and it's argument the first one is current stage and nex one is address of page and then we can get two string or nothing the
    // first string is username of input user and second one is type of user
    private void createUsersPage(Stage stage , String location , String...username) {
        //close previous page
        stage.close();
        //load new page
        FXMLLoader loader = new FXMLLoader(Main.class.getResource(location));
        try {
            loader.load();
        } catch (IOException e) {
            System.out.println("we are in login page controller class in create user page method" + e.getMessage());
        }
        //create root
        AnchorPane root = loader.getRoot();
        //if user was manger or employee or passenger method pass the username input to it's controller class
        if(username.length !=0) {
            if (username[1].equals("manager")) {
                ManagerPageController controller = loader.getController();
                controller.setUsername(username[0]);
            } else if (username[1].equals("employee")) {
                EmployeePageController controller = loader.getController();
                controller.setUsername(username[0]);
            } else if (username[1].equals("passenger")) {
                PassengerPageController controller = loader.getController();
                controller.setUsername(username[0]);
            }
        }
        //set style
        stage = new Stage(StageStyle.DECORATED);
        //set scene
        Scene scene = new Scene(root);
        //get position of mouse
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        //set position of mouse
        Stage finalStage = stage;
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                finalStage.setX(event.getScreenX() - xOffset);
                finalStage.setY(event.getScreenY() - yOffset);
            }
        });
        //set scene
        stage.setScene(scene);
        stage.show();
    }

    //this method check input username belongs to managers or do not
   public boolean checkManager(String username){
       for (int i = 0; i < Manager.managers.size(); i++) {
           if(Manager.managers.get(i).getUserName().equals(username)){
               //find username
               return true;
           }
       }
       return false;
   }

   //this method return index of manager that username belongs to him\her
   public int indexManager(String username){
        int index = 0;
       for (int i = 0; i < Manager.managers.size(); i++) {
           if(Manager.managers.get(i).getUserName().equals(username)){
               //found index
              index = i;
              break;

           }
       }
       return index;
   }


    //this method check input username belongs to employees or do not
    private boolean checkEmployee(String username) {
        for (int i = 0; i < Employee.employees.size(); i++) {
            if(Employee.employees.get(i).getUserName().equals(username)){
                //found username
                return true;
            }
        }
        return false;
    }


    //this method return index of manager that username belongs to him\her
    public int indexEmployee(String username){
        int index = 0;
        for (int i = 0; i < Employee.employees.size(); i++) {
            if(Employee.employees.get(i).getUserName().equals(username)){
                //found index
                index = i;
                break;

            }
        }
        return index;
    }
}
