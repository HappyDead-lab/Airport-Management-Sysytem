package controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Employee;
import model.Passenger;
import java.net.URL;
import java.util.ResourceBundle;


public class ShowMessagePageController implements Initializable {
    private @FXML TableView<String> employeeTableView;
    private @FXML TableView<String> passengerTableView;
    private @FXML TableColumn<String,String> employeeMessage;
    private @FXML TableColumn<String,String> passengerMessage;
    private @FXML Button exitBTN;

    //set on action for remove employee message button
    public void setOnActionRemoveEmployeeMessageBTN(ActionEvent event){
        //get all information from table view
        ObservableList<String> selectedMessage = null , allMessages;
        allMessages = employeeTableView.getItems();
        try {
            //get selection row of employee table view
            selectedMessage = employeeTableView.getSelectionModel().getSelectedItems();
        }catch (NullPointerException e){
            System.out.println("Please select a row " + e.getMessage());
        }
        //here we find selection message from employee table view and remove it from file and array list
        for (int i = 0; i < Employee.employeesMessage.size(); i++) {
            if(Employee.employeesMessage.get(i).equals(selectedMessage.get(0))){
                //found it
                Employee.employeesMessage.remove(i);
                MessagePageController.writeFileEmployee();
                break;
            }
        }

        //remove selection row from on of table views
        selectedMessage.forEach(allMessages::remove);

    }

    //set on action for remove passenger message button
    public void setOnActionRemovePassengerMessageBTN(ActionEvent event){
        //get all information from table view
        ObservableList<String> selectedMessage = null , allMessages;
        allMessages = passengerTableView.getItems();
        try {
            //get selection row of passenger table view
            selectedMessage = passengerTableView.getSelectionModel().getSelectedItems();
        }catch (NullPointerException e){
            System.out.println("Please select a row " + e.getMessage());
        }
        //here we find selection message from passenger table view and remove it from file and array list
        for (int i = 0; i < Passenger.passengersMessage.size(); i++) {
            if(Passenger.passengersMessage.get(i).equals(selectedMessage.get(0))){
                //found it
                Passenger.passengersMessage.remove(i);
                MessagePageController.writeFilePassenger();
                break;
            }
        }

        //remove selection row from on of table views
        selectedMessage.forEach(allMessages::remove);

    }


    //set on action for exit button
    public void setOnActionExitBTN(ActionEvent event){
        //get current stage
        Stage stage = (Stage)exitBTN.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        employeeMessage.setCellValueFactory(new PropertyValueFactory<>("employeeMessages"));
        employeeMessage.setCellValueFactory(data -> new SimpleStringProperty(data.getValue()));

        passengerMessage.setCellValueFactory(new PropertyValueFactory<>("passengerMessages"));
        passengerMessage.setCellValueFactory(data -> new SimpleStringProperty(data.getValue()));

        //add messages to passenger table view
        for (int i = 0; i < Passenger.passengersMessage.size(); i++) {
            passengerTableView.getItems().add(Passenger.passengersMessage.get(i));
        }
        //add messages to employee table view
        for (int i = 0; i < Employee.employeesMessage.size(); i++) {
            employeeTableView.getItems().add(Employee.employeesMessage.get(i));
        }



    }
}
