package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import model.Employee;
import model.Passenger;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class MessagePageController implements Initializable {
    private @FXML TextArea ideaBoxTXA;
    private @FXML Button sendBTN;
    private @FXML Button cancelBTN;
    //this variable determine user is a employee or not
    private boolean isEmployee = false;

    //this variable determine user is a passenger or not
    private boolean isPassenger = false;

    public void setEmployee(boolean employee) {
        isEmployee = employee;
    }

    public void setPassenger(boolean passenger) {
        isPassenger = passenger;
    }



    //set on action for cancel button
    public void setOnActionCancelBTN(ActionEvent event){
        Stage stage = (Stage)cancelBTN.getScene().getWindow();
        stage.close();
    }

    //set on action for clear button
    public void setOnActionClearBTN(ActionEvent event){
        ideaBoxTXA.clear();
    }

    //set on action for send button
    public void setOnActionSendBTN(ActionEvent event){
        Stage stage = (Stage) sendBTN.getScene().getWindow();
        if(ideaBoxTXA.getText().equals("")){
            showError(stage);
        }else{
            if(isEmployee) {
                //save it
                Employee.employeesMessage.add(ideaBoxTXA.getText());
                //save it in file
                writeFileEmployee();
                ideaBoxTXA.clear();
                stage.close();
            }else if (isPassenger){
                //save it in array list
                Passenger.passengersMessage.add(ideaBoxTXA.getText());
                //save it in array list
                writeFilePassenger();
                ideaBoxTXA.clear();
                stage.close();
            }

        }
    }

    //this method show an appropriate error
    private void showError(Stage stage) {
        ErrorPageController.text = "Please write your opinion";
        ErrorPageController.createErrorWindow();
    }

    //this method write messages from employee in file
    public static void writeFileEmployee()  {
        FileWriter fileWriter = null;

        BufferedWriter bufferedWriter = null;
        try {
            FileWriter fwOb = new FileWriter("employees message.txt", false);
            PrintWriter pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();

            fileWriter = new FileWriter("employees message.txt");
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        for (int i = 0; i < Employee.employeesMessage.size(); i++) {
            String saveMessage = Employee.employeesMessage.get(i) + "/";
            try {
                //save all messages in file
                bufferedWriter.write(saveMessage);
                bufferedWriter.newLine();
            }catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }


    //this method read messages from file in employee array list
    public static void readFileEmployee() throws IOException {
        FileReader fileReader = new FileReader("employees message.txt");
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String str;
        while ((str = bufferedReader.readLine()) != null) {
            String[] messages = str.split("/");
            for (String message : messages) {
               Employee.employeesMessage.add(message);
            }

        }

    }


    //this method write messages from employee in file
    public static void writeFilePassenger()  {
        FileWriter fileWriter = null;

        BufferedWriter bufferedWriter = null;
        try {
            FileWriter fwOb = new FileWriter("passengers message.txt", false);
            PrintWriter pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();

            fileWriter = new FileWriter("passengers message.txt");
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        for (int i = 0; i < Passenger.passengersMessage.size(); i++) {
            String saveMessage = Passenger.passengersMessage.get(i) + "/";
            try {
                //save all messages in file
                bufferedWriter.write(saveMessage);
                bufferedWriter.newLine();
            }catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }


    //this method read messages from file in passenger array list
    public static void readFilePassenger() throws IOException {
        FileReader fileReader = new FileReader("passengers message.txt");
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String str;
        while ((str = bufferedReader.readLine()) != null) {
            String[] messages = str.split("/");
            for (String message : messages) {
                Passenger.passengersMessage.add(message);
            }

        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //below line is for prevent of getting null variables
        Platform.runLater(() ->{

        });
    }
}
