package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Employee;
import model.Manager;
import model.Passenger;


import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class ForgetPasswordController implements Initializable {
    private @FXML ChoiceBox<String> forgetPasswordBox;
    private @FXML Button enterBTN;
    private @FXML Button cancelBTN;
    private @FXML TextField answerTXF;
    private @FXML TextField emailTXF;
    private @FXML PasswordField passwordPSF;
    private @FXML PasswordField confirmationPSF;
    //these variables determine user type
    private boolean isManager = false;
    private boolean isEmployee = false;
    private boolean isPassenger = false;
    private boolean isSuperAdmin = false;
    //these variable for make first question of security questions
    private Random rand = new Random();
    private int num = rand.nextInt(100);
    private int num1 = rand.nextInt(100);

    //make security question
    private String questionOne = Integer.toString(num) + " + " + Integer.toString(num1) + " = ";
    private String questionTwo = "What country do you live ?";
    private String questionThree = "What year are we ?";
    private String firstValue = "Select...";

    //this method is for initialize choice box
    private void setForgetPasswordBox() {


        //add questions to choice box
        forgetPasswordBox.getItems().addAll(firstValue, questionOne, questionTwo, questionThree);
        //set first value to show in the choice box
        forgetPasswordBox.setValue(firstValue);

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setForgetPasswordBox();
    }

    // set on action for enter button
    public void setEnterBTN(ActionEvent event) throws IOException {
        LoginPageController.read();
        String savePass = LoginPageController.passWord;
        //get current Stage
        final Stage stage = (Stage) enterBTN.getScene().getWindow();

        if (emailTXF.getText().equals("hamed")) {
            isSuperAdmin = true;
            //choice box and answer text field will be visible
            forgetPasswordBox.setDisable(false);
            answerTXF.setDisable(false);
            //make enter text field disable
            emailTXF.setDisable(true);

            //set on action for enter button after user answer security question
            enterBTN.setOnAction(event1 -> processOfForgetPassword(stage));

        }
        isManager = checkManager(emailTXF.getText());
        if(isManager){
            //choice box and answer text field will be visible
            forgetPasswordBox.setDisable(false);
            answerTXF.setDisable(false);
            //make enter text field disable
            emailTXF.setDisable(true);

            //set on action for enter button after user answer security question
            enterBTN.setOnAction(event1 -> processOfForgetPassword(stage));

        }

        isEmployee = checkEmployee(emailTXF.getText());
        if(isEmployee){
            //choice box and answer text field will be visible
            forgetPasswordBox.setDisable(false);
            answerTXF.setDisable(false);
            //make enter text field disable
            emailTXF.setDisable(true);

            //set on action for enter button after user answer security question
            enterBTN.setOnAction(event1 -> processOfForgetPassword(stage));

        }

        isPassenger = checkPassenger(emailTXF.getText());
        if(isPassenger){
            //choice box and answer text field will be visible
            forgetPasswordBox.setDisable(false);
            answerTXF.setDisable(false);
            //make enter text field disable
            emailTXF.setDisable(true);

            //set on action for enter button after user answer security question
            enterBTN.setOnAction(event1 -> processOfForgetPassword(stage));

        }

        //if username text field was empty or username does not exist
        if(!(isEmployee || isManager || isPassenger || isSuperAdmin) || emailTXF.getText().equals("")){
            showError("Please enter correct email");
        }
    }

    //this method check input username belongs to passenger or do not
    private boolean checkPassenger(String email) {
        for (int i = 0; i < Passenger.passengers.size(); i++) {
            if(Passenger.passengers.get(i).getEmail().equals(email)){
                //found email
                return true;
            }
        }
        return false;
    }

    //this method check input username belongs to employees or do not
    private boolean checkEmployee(String email) {
        for (int i = 0; i < Employee.employees.size(); i++) {
            if(Employee.employees.get(i).getEmail().equals(email)){
                //found email
                return true;
            }
        }
        return false;
    }


    //this method return index of manager that username belongs to him\her
    private int indexEmployee(String email){
        int index = 0;
        for (int i = 0; i < Employee.employees.size(); i++) {
            if(Employee.employees.get(i).getEmail().equals(email)){
                //found index
                index = i;
                break;

            }
        }
        return index;
    }


    //this method check that input email belongs to a manager or does not
    private boolean checkManager(String email) {
        for (int i = 0; i < Manager.managers.size(); i++) {
            if(Manager.managers.get(i).getEmail().equals(email)){
                //found email
                return true;
            }
        }
        return false;
    }

    //this method return the index of manager that we want to change password
    private int indexManager(String email) {
        int saveIndex = 0;
        for (int i = 0; i < Manager.managers.size(); i++) {
            if(Manager.managers.get(i).getEmail().equals(email)){
                //found index
                saveIndex = i;
            }
        }
        return saveIndex;
    }
    //this method return the index of passenger that we want to change password
    private int indexPassenger(String email) {
        int saveIndex = 0;
        for (int i = 0; i < Passenger.passengers.size(); i++) {
            if(Passenger.passengers.get(i).getEmail().equals(email)){
                //found index
                saveIndex = i;
            }
        }
        return saveIndex;
    }


    //this method show error if email field is empty
    private void showError(String message) {
        ErrorPageController.text = message ;
        ErrorPageController.createErrorWindow();
    }
    //this method execute when user fill the email field correctly
    private void processOfForgetPassword(Stage stage) {
        {

            //get choice value from choice box
            String value = forgetPasswordBox.getValue();

            //if the value be Select
            if (value.equals(firstValue)) {
                showError("Please select a question from choice box");
            }
            //if the value be the first question
            if (value.equals(questionOne)) {
                if (answerTXF.getText().equals(Integer.toString(num1 + num))) {
                    //choice box and answer text field will be disable
                    forgetPasswordBox.setDisable(true);
                    answerTXF.setDisable(true);

                    //make both password fields visible
                    confirmationPSF.setVisible(true);
                    passwordPSF.setVisible(true);
                    enterBTN.setOnAction(event2 -> {
                        //get current window
                        Stage stage1 = (Stage) enterBTN.getScene().getWindow();

                        //show appropriate error
                        if (passwordPSF.getText().equals("") || confirmationPSF.getText().equals("")) {
                            //if at least one of field is empty
                            showError("Please fill all of the fields");
                        } else if (!(passwordPSF.getText().equals(confirmationPSF.getText()))) {
                            //if new password is not equal to its confirmation
                            showError("Please enter correct confirmation");
                        } else {
                            boolean isPass = Check.checkPassword(passwordPSF.getText());
                            if (isPass) {
                                //if user was a passenger
                                if(isPassenger){
                                    setPasswordPassenger(stage1);
                                    return;
                                }
                                //if user was a employee
                                if(isEmployee){
                                    setPasswordEmployee(stage1);
                                    return;
                                }
                                //if user was a manager
                                if (isManager) {
                                    setPasswordManager(stage1);
                                    return;
                                }
                                //if user was super admin
                                LoginPageController.passWord = passwordPSF.getText();
                                try {
                                    LoginPageController.write();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                stage1.close();
                            }else{
                               showError("It is less than 5 character");
                            }
                        }
                    });

                } else {
                    //if user does not answer security question error window will open
                    //set error message
                    showError("Please fill all of the field carefully");
                }


            }
            if (value.equals(questionTwo)) {
                if (answerTXF.getText().equals("Iran")) {
                    //choice box and answer text field will be disable
                    forgetPasswordBox.setDisable(true);
                    answerTXF.setDisable(true);

                    //make both password fields visible
                    confirmationPSF.setVisible(true);
                    passwordPSF.setVisible(true);


                    enterBTN.setOnAction(event2 -> {
                        //get current window
                        Stage stage1 = (Stage) enterBTN.getScene().getWindow();

                        //show appropriate error
                        if (passwordPSF.getText().equals("") || confirmationPSF.getText().equals("")) {
                            //if at least one of field is empty
                            //set error message
                           showError("Please fill all of the fields");
                        } else if (!(passwordPSF.getText().equals(confirmationPSF.getText()))) {
                            //if new password is not equal to its confirmation
                           showError("Please enter correct confirmation");
                        } else {
                            boolean isPass = Check.checkPassword(passwordPSF.getText());
                            if (isPass) {
                                //if user was a passenger
                                if(isPassenger){
                                    setPasswordPassenger(stage1);
                                    return;
                                }
                                //if user was a employee
                                if(isEmployee){
                                    setPasswordEmployee(stage1);
                                    return;
                                }

                                //if user was a manager
                                if (isManager) {
                                    setPasswordManager(stage1);
                                    return;
                                }
                                //if user was super admin
                                LoginPageController.passWord = passwordPSF.getText();
                                try {
                                    LoginPageController.write();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                stage1.close();
                            }else{
                               showError("It is less than 5 character");
                            }
                        }
                    });
                } else {
                   showError("Please fill all of the field carefully");
                }

            }
            if (value.equals(questionThree)) {
                if (answerTXF.getText().equals("2020")) {
                    //choice box and answer text field will be disable
                    forgetPasswordBox.setDisable(true);
                    answerTXF.setDisable(true);

                    //make both password fields visible
                    confirmationPSF.setVisible(true);
                    passwordPSF.setVisible(true);


                    enterBTN.setOnAction(event2 -> {
                        //get current window
                        Stage stage1 = (Stage) enterBTN.getScene().getWindow();

                        //show appropriate error
                        if (passwordPSF.getText().equals("") || confirmationPSF.getText().equals("")) {
                            //if at least one of field is empty
                            showError("Please fill all of the fields");
                        } else if (!(passwordPSF.getText().equals(confirmationPSF.getText()))) {
                            //if new password is not equal to its confirmation
                            showError("Please enter correct confirmation");
                        } else {
                            boolean isPass = Check.checkPassword(passwordPSF.getText());
                            if (isPass) {
                                //if user was a passenger
                                if(isPassenger){
                                    setPasswordPassenger(stage1);
                                    return;
                                }
                                //if user was a employee
                                if(isEmployee){
                                    setPasswordEmployee(stage1);
                                    return;
                                }

                                //if user was a manager
                                if (isManager) {
                                    setPasswordManager(stage1);
                                    return;
                                }
                                //if user was super admin
                                LoginPageController.passWord = passwordPSF.getText();
                                try {
                                    LoginPageController.write();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                stage1.close();
                            }else{
                               showError("It is less than 5 character");
                            }
                        }
                    });
                } else {
                   showError("Please fill all of the field carefully");
                }
            }

        }
    }
    //this method update the passenger password
    private void setPasswordPassenger(Stage stage1) {
        String pass = "";
        int index;
        //get index of manager
        index = indexPassenger(emailTXF.getText());
        pass = passwordPSF.getText();
        //set password for array list
        Passenger.passengers.get(index).setPassword(pass);
        //set password to database
        Database.changePassword(new Passenger(),Passenger.passengers.get(index).getUserName(), pass);
        stage1.close();
    }

    //this method update the manager password
    private void setPasswordManager(Stage stage1) {
            String pass = "";
            int index;
            //get index of manager
            index = indexManager(emailTXF.getText());
            pass = passwordPSF.getText();
            //set password for array list
            Manager.managers.get(index).setPassword(pass);
            //set password to database
            Database.changePassword(new Manager(),Manager.managers.get(index).getUserName(), pass);
            stage1.close();

    }

    //this method update the employee password
    private void setPasswordEmployee(Stage stage1) {
        String pass = "";
        int index;
        //get index of manager
        index = indexEmployee(emailTXF.getText());
        pass = passwordPSF.getText();
        //set password for array list
        Employee.employees.get(index).setPassword(pass);
        //set password to database
        Database.changePassword(new Employee(),Employee.employees.get(index).getUserName(), pass);
        stage1.close();

    }

    //set on action for cancel button
    public void setCancelBTN(ActionEvent event) {
        Stage stage = (Stage) cancelBTN.getScene().getWindow();
        stage.close();
    }

}
