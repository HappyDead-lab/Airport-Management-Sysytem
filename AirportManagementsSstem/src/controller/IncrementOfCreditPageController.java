package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Passenger;

import java.net.URL;
import java.util.ResourceBundle;

public class IncrementOfCreditPageController implements Initializable {
    private @FXML TextField creditTXF;
    private @FXML Button cancelBTN;
    private @FXML Button incrementOfCreditBTN;

    //save username input
    private String saveUsername;

    public void setSaveUsername(String saveUsername) {
        this.saveUsername = saveUsername;
    }

    //set on action for cancel button
    public void setOnActionCancelBTN(ActionEvent event){
        //get current window
        Stage stage = (Stage)cancelBTN.getScene().getWindow();
        stage.close();
    }


    //set on action for increase button
    public void setOnActionIncreaseBTN(ActionEvent event){
        Stage stage = (Stage)incrementOfCreditBTN.getScene().getWindow();
        if(creditTXF.getText().equals("")){
            ErrorPageController.text = "Please fill the field";
            ErrorPageController.createErrorWindow();
        }else{
            //here we find the passenger and set it's new credit
            for (int i = 0; i < Passenger.passengers.size(); i++) {
                if(Passenger.passengers.get(i).getUserName().equals(saveUsername)){
                    //found it
                    int newCredit = Integer.parseInt(Passenger.passengers.get(i).getCredit()) + Integer.parseInt(creditTXF.getText());
                    //set new credit
                    Passenger.passengers.get(i).setCredit(Integer.toString(newCredit));
                    //update database
                    Database.updateInformation(Passenger.passengers.get(i),Passenger.passengers.get(i).getID());
                    break;
                }
            }
            stage.close();
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //below line is for prevent of getting null variables
        Platform.runLater(() ->{

        });
    }
}
