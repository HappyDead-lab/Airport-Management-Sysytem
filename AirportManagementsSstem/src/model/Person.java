package model;

import javafx.beans.property.SimpleStringProperty;

public class Person implements Showable{
   private SimpleStringProperty ID;
   private SimpleStringProperty name;
   private SimpleStringProperty lastName;
   private SimpleStringProperty userName;
   private SimpleStringProperty password;
   private SimpleStringProperty email;
   private SimpleStringProperty phoneNumber;

    public Person() {

    }

    public Person(String ID, String name, String lastName, String userName, String password, String email , String phoneNumber) {
        this.ID = new SimpleStringProperty(ID);
        this.name = new SimpleStringProperty(name);
        this.lastName = new SimpleStringProperty(lastName);
        this.userName = new SimpleStringProperty(userName);
        this.password = new SimpleStringProperty(password);
        this.email = new SimpleStringProperty(email);
        this.phoneNumber = new SimpleStringProperty(phoneNumber);
    }

    public String getID() {
        return ID.get();
    }

    public void setID(String ID) {
        this.ID = new SimpleStringProperty(ID);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name = new SimpleStringProperty(name);
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName = new SimpleStringProperty(lastName);
    }

    public String getUserName() {
        return userName.get();
    }

    public void setUserName(String userName) {
        this.userName =  new SimpleStringProperty(userName);
    }

    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password = new SimpleStringProperty(password);
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email = new SimpleStringProperty(email);
    }

    public String getPhoneNumber() {
        return phoneNumber.get();
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = new SimpleStringProperty(phoneNumber);
    }

    //this method show information of Children class
    @Override
    public void show() {
        System.out.println("ID : " + this.ID.get());
        System.out.println("First Name : " + this.name.get());
        System.out.println("Last Name : " + this.lastName.get());
        System.out.println("Username : " + this.userName.get());
        System.out.println("Password : " + this.password.get());
        System.out.println("Email : " + this.email.get());
        System.out.println("Phone Number : " + this.phoneNumber.get());
    }
}
