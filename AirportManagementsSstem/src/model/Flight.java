package model;

import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;

public class Flight implements Showable{
    private SimpleStringProperty ID;
    private SimpleStringProperty airplaneID ;
    private ArrayList<Ticket> tickets = new ArrayList<>();
    private SimpleStringProperty origin;
    private SimpleStringProperty destiny;
    private SimpleStringProperty dateToFight;
    private SimpleStringProperty timeToFight;
    private SimpleStringProperty numberOfSoldTicket;
    private ArrayList<Passenger> passengers = new ArrayList<>();
    private SimpleStringProperty takenTime;
    public static ArrayList<Flight> flights = new ArrayList<>();

    public String getID() {
        return ID.get();
    }


    public void setID(String ID) {
        this.ID = new SimpleStringProperty(ID);
    }

    public ArrayList<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(ArrayList<Ticket> tickets) {
        this.tickets = tickets;
    }

    public String getOrigin() {
        return origin.get();
    }

    public void setOrigin(String origin) {
        this.origin = new SimpleStringProperty(origin);
    }

    public String getDestiny() {
        return destiny.get();
    }

    public void setDestiny(String destiny) {
        this.destiny = new SimpleStringProperty(destiny);
    }

    public String getDateToFight() {
        return dateToFight.get();
    }

    public void setDateToFight(String dateToFight) {
        this.dateToFight = new SimpleStringProperty(dateToFight);
    }

    public String getTimeToFight() {
        return timeToFight.get();
    }

    public void setTimeToFight(String timeToFight) {
        this.timeToFight = new SimpleStringProperty(timeToFight);
    }

    public String getNumberOfSoldTicket() {
        return numberOfSoldTicket.get();
    }

    public void setNumberOfSoldTicket(String numberOfSoldTicket) {
        this.numberOfSoldTicket = new SimpleStringProperty(numberOfSoldTicket);
    }

    public ArrayList<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(ArrayList<Passenger> passengers) {
        this.passengers = passengers;
    }

    public String getTakenTime() {
        return takenTime.get();
    }

    public void setTakenTime(String takenTime) {
        this.takenTime = new SimpleStringProperty(takenTime);
    }

    public String getAirplaneID() {
        return airplaneID.get();
    }

    public void setAirplaneID(String airplaneID) {
        this.airplaneID = new SimpleStringProperty(airplaneID);
    }
    public Flight(){

    }

    public Flight(String ID, String origin, String destiny, String dateToFight, String timeToFight, String numberOfSoldTicket,  String takenTime, String airplaneID) {
        this.ID =new SimpleStringProperty(ID);
        this.airplaneID = new SimpleStringProperty(airplaneID);
        this.origin = new SimpleStringProperty(origin);
        this.destiny =new SimpleStringProperty(destiny);
        this.dateToFight = new SimpleStringProperty(dateToFight);
        this.timeToFight =new SimpleStringProperty(timeToFight);
        this.numberOfSoldTicket = new SimpleStringProperty(numberOfSoldTicket);
        this.takenTime = new SimpleStringProperty(takenTime);
    }
    //this method show information of Flight object
    @Override
    public void show() {
        System.out.println("ID : " + this.ID.get());
        System.out.println("Origin : " + this.origin.get());
        System.out.println("Destiny : " + this.destiny.get());
        System.out.println("Date To Flight : " + this.dateToFight.get());
        System.out.println("Time To Flight : " + this.timeToFight.get());
        System.out.println("Taken Time : " + this.takenTime.get());
        System.out.println("Number Of Sold Tickets : " + this.numberOfSoldTicket.get());
        System.out.println("AirplaneID : " + this.airplaneID.get());
        System.out.println();
        System.out.println();

    }
}
