package model;

import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;

public class Airplane implements Showable {
    private SimpleStringProperty ID;
    private SimpleStringProperty numberOfSeats;
    private ArrayList<Flight> flightList ;
    public static ArrayList<Airplane> airplanes = new ArrayList<>();

    public Airplane(){

    }

    public Airplane(String ID, String numberOfSeats, ArrayList<Flight> flightList) {
        this.ID = new SimpleStringProperty(ID);
        this.numberOfSeats = new SimpleStringProperty(numberOfSeats);
        this.flightList = flightList;
    }

    public Airplane(String ID, String numberOfSeats) {
        this.ID = new SimpleStringProperty(ID);
        this.numberOfSeats = new SimpleStringProperty(numberOfSeats);
    }

    public String getID() {
        return ID.get();
    }

    public void setID(String ID) {
        this.ID = new SimpleStringProperty(ID);
    }

    public String getNumberOfSeats() {
        return numberOfSeats.get();
    }

    public void setNumberOfSeats(String numberOfSeats) {
        this.numberOfSeats = new SimpleStringProperty(numberOfSeats);
    }

    public ArrayList<Flight> getFlightList() {
        return flightList;
    }

    public void setFlightList(ArrayList<Flight> flightList) {
        this.flightList = flightList;
    }

    //this method show information of Airplane object
    @Override
    public void show() {
        System.out.println("ID : " + this.ID.get());
        System.out.println("Number Of Seats : " + this.numberOfSeats.get());
        System.out.println();
        System.out.println();
    }
}
