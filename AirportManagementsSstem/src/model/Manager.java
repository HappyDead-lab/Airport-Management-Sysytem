package model;

import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;

public class Manager extends Person{
    public static ArrayList<Manager> managers = new ArrayList<>();
    private SimpleStringProperty address;
    private SimpleStringProperty salary;

    public Manager() {
        super();
    }

    public Manager(String ID, String name, String lastName, String userName, String password,  String phoneNumber, String address,String email, String salary) {
        super(ID, name, lastName, userName, password, email,phoneNumber);
        this.address = new SimpleStringProperty(address);
        this.salary = new SimpleStringProperty(salary);
    }


    public String getAddress() {
        return address.get();
    }

    public void setAddress(String address) {
        this.address = new SimpleStringProperty(address);
    }

    public String getSalary() {
        return salary.get();
    }

    public void setSalary(String salary) {
        this.salary = new SimpleStringProperty(salary);
    }

    //this method show information of Manager object
    @Override
    public void show() {
        super.show();
        System.out.println("Address : " + this.address.get());
        System.out.println("Salary : " + salary.get());
        System.out.println();
        System.out.println();
    }


}
