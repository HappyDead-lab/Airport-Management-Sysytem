package model;

import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;

public class Passenger extends Person {
    private SimpleStringProperty credit;
    private ArrayList<Ticket> tickets = new ArrayList<>();

    public ArrayList<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(ArrayList<Ticket> tickets) {
        this.tickets = tickets;
    }

    //for save all passengers
    public static ArrayList<Passenger> passengers = new ArrayList<>();
    //declare array list for save message that every passenger send to managers
    public static ArrayList<String> passengersMessage = new ArrayList<>();

    public Passenger(){
        super();
    }




    public Passenger(String ID, String name, String lastName, String userName, String password, String phoneNumber, String email, String credit) {
        super(ID, name, lastName, userName, password, email,phoneNumber);
       this.credit = new SimpleStringProperty(credit);
    }

    public String getCredit() {
        return credit.get();
    }


    public void setCredit(String credit) {
        this.credit = new SimpleStringProperty(credit);
    }

    //this method show information of Passenger object
    @Override
    public void show() {
        super.show();
        System.out.println("Credit : " + this.credit.get());
        System.out.println();
        System.out.println();
    }
}
