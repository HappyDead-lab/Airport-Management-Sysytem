package model;

import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;

public class Employee extends Person {
    private SimpleStringProperty address;
    private SimpleStringProperty salary;
    //for save all employees
    public static ArrayList<Employee> employees = new ArrayList<>();
    //declare array list for save message that every employee send to managers
    public static ArrayList<String> employeesMessage = new ArrayList<>();


    public String getAddress() {
        return address.get();
    }

    public void setAddress(String address) {
        this.address = new SimpleStringProperty(address);
    }

    public String getSalary() {
        return salary.get();
    }

    public void setSalary(String salary) {
        this.salary = new SimpleStringProperty(salary);
    }

    public Employee() {
       super();
    }

    public Employee(String ID, String name, String lastName, String userName, String password,  String phoneNumber, String address,String email, String salary) {
        super(ID, name, lastName, userName, password, email,phoneNumber);
        this.address = new SimpleStringProperty(address);
        this.salary = new SimpleStringProperty(salary);
    }

    //this method show information of Employee object
    @Override
    public void show() {
        super.show();
        System.out.println("Address : " + this.address.get());
        System.out.println("Salary : " + salary.get());
        System.out.println();
        System.out.println();
    }


}
