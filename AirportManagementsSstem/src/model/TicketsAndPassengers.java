package model;

import java.util.ArrayList;

public class TicketsAndPassengers {
    private String ticketID;
    public static ArrayList<TicketsAndPassengers> ticketsAndPassengers = new ArrayList<>();

    public TicketsAndPassengers() {
    }

    public TicketsAndPassengers(String passengerID, String ticketID) {
        this.ticketID = ticketID;
        PassengerID = passengerID;
    }

    public String getTicketID() {
        return ticketID;
    }

    public void setTicketID(String ticketID) {
        this.ticketID = ticketID;
    }

    public String getPassengerID() {
        return PassengerID;
    }

    public void setPassengerID(String passengerID) {
        PassengerID = passengerID;
    }

    private String PassengerID;
}
