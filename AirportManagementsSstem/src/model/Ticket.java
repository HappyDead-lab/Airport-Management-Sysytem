package model;

import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;

public class Ticket implements Showable{
    private SimpleStringProperty ID;
    private SimpleStringProperty price;
    private SimpleStringProperty priceOfCancelling;
    private SimpleStringProperty flightID;
    private ArrayList<Passenger> passengers = new ArrayList<>();

    public ArrayList<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(ArrayList<Passenger> passengers) {
        this.passengers = passengers;
    }

    public static ArrayList<Ticket> tickets = new ArrayList<>();

    public String getFlightID() {
        return flightID.get();
    }
    public void setFlightID(String flightID) {
        this.flightID = new SimpleStringProperty(flightID);
    }

    public Ticket(){

    }
    public Ticket(String ID, String price, String priceOFCancelling, String flightID) {
        this.ID = new SimpleStringProperty(ID);
        this.price = new SimpleStringProperty(price);
        this.priceOfCancelling = new SimpleStringProperty(priceOFCancelling);
        this.flightID = new SimpleStringProperty(flightID);

    }

    public String getID() {
        return ID.get();
    }


    public void setID(String ID) {
        this.ID = new SimpleStringProperty(ID);
    }

    public String getPrice() {
        return price.get();
    }

    public void setPrice(String price) {
        this.price = new SimpleStringProperty(price);
    }

    public String getPriceOfCancelling() {
        return priceOfCancelling.get();
    }

    public void setPriceOfCancelling(String priceOfCancelling) {
        this.priceOfCancelling = new SimpleStringProperty(priceOfCancelling);
    }

    @Override
    public void show() {
        System.out.println("ID : " + ID.get());
        System.out.println("Price : " + price.get());
        System.out.println("Price Of Cancelling : " + priceOfCancelling.get());
        System.out.println("Flight ID : " + flightID.get());
        System.out.println();
        System.out.println();

    }
}
