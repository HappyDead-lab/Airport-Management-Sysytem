package Main;

import controller.*;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.*;

import java.io.IOException;
import java.util.ArrayList;


public class Main extends Application {

    //this variables help that make window movable
    private double xOffset = 0;
    private double yOffset = 0;

    @Override
    public void start(Stage primaryStage) throws Exception {

        //create root and load page
        AnchorPane root = FXMLLoader.load(getClass().getResource("../view/LoginPage.fxml"));
        //set style for primary stage
        primaryStage = new Stage(StageStyle.UNDECORATED);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        //creae scene
        Scene scene = new Scene(root);
        //set style for scene
        scene.setFill(Color.TRANSPARENT);
        //read super admin password from file
        LoginPageController.read();

        //get position of mouse
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        Stage finalPrimaryStage = primaryStage;
        //set position of mouse
        root.setOnMouseDragged(event -> {
            finalPrimaryStage.setX(event.getScreenX() - xOffset);
            finalPrimaryStage.setY(event.getScreenY() - yOffset);
        });
        //set scene
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    //this method find all tickets with particular flight ID
    public static ArrayList<Ticket> findTickets(String ID) {
        ArrayList<Ticket> ticketsForID = new ArrayList<>();

        for (int i = 0; i < Ticket.tickets.size(); i++) {
            if (Ticket.tickets.get(i).getFlightID().equals(ID)) {
                ticketsForID.add(Ticket.tickets.get(i));
            }
        }

        return ticketsForID;

    }

    //this method find all passengers with particular flight ID
    public static ArrayList<Passenger> findPassengers(String ID) {
        ArrayList<Passenger> passengersForID = new ArrayList<>();

        for (int i = 0; i < Passenger.passengers.size(); i++) {
            for (int j = 0; j < Passenger.passengers.get(i).getTickets().size(); j++) {
                for (int k = 0; k < Ticket.tickets.size(); k++) {
                    if(Ticket.tickets.get(k).getID().equals(Passenger.passengers.get(i).getTickets().get(j).getID())){
                            if(Passenger.passengers.get(i).getTickets().get(j).getFlightID().equals(ID)){
                                passengersForID.add(Passenger.passengers.get(i));
                            }

                    }
                }
            }
        }
        return passengersForID;
    }

    //this method find all flights with particular airplane ID
    public static ArrayList<Flight> findFlights(String ID){
        ArrayList<Flight> flightsForID = new ArrayList<>();

        for (int i = 0; i < Flight.flights.size(); i++) {
            if(Flight.flights.get(i).getAirplaneID().equals(ID)){
                flightsForID.add(Flight.flights.get(i));
            }
        }
        return flightsForID;
    }


    //save tickets as a list to a variable of Flight class
    public static void setTicketsAndPassengers(){
        clearTicketsAndPassengers();
        for (int i = 0; i < TicketsAndPassengers.ticketsAndPassengers.size(); i++) {
            for (int j = 0; j < Passenger.passengers.size(); j++) {
                if(TicketsAndPassengers.ticketsAndPassengers.get(i).getPassengerID().equals(Passenger.passengers.get(j).getID())){
                    for (int k = 0; k < Ticket.tickets.size(); k++) {
                        if(TicketsAndPassengers.ticketsAndPassengers.get(i).getTicketID().equals(Ticket.tickets.get(k).getID())){
                            Passenger.passengers.get(j).getTickets().add(Ticket.tickets.get(k));
                            Ticket.tickets.get(k).getPassengers().add(Passenger.passengers.get(j));
                        }
                    }
                }
            }
        }
    }
    //this method clear list of tickets and passengers
    private static void clearTicketsAndPassengers(){
        for (int i = 0; i < Passenger.passengers.size(); i++) {
                Passenger.passengers.get(i).getTickets().clear();
        }

        for (int i = 0; i < Ticket.tickets.size(); i++) {
            Ticket.tickets.get(i).getPassengers().clear();
        }
    }

    //save flights as a list to a variable of Airplane class
    public static void setFlights(){
        for (int i = 0; i < Airplane.airplanes.size(); i++) {
            Airplane.airplanes.get(i).setFlightList(findFlights(Airplane.airplanes.get(i).getID()));
        }
    }

    //save passenger as a list to a variable of Flight class
    public static void setPassengers(){
        for (int i = 0; i < Flight.flights.size(); i++) {
            Flight.flights.get(i).setPassengers(findPassengers(Flight.flights.get(i).getID()));
        }
    }
    //this method save tickets as a list to a variable of Flight class
    public static void setTickets(){
        for (int i = 0; i < Flight.flights.size(); i++) {
            Flight.flights.get(i).setTickets(findTickets(Flight.flights.get(i).getID()));
        }
    }


    public static void main(String[] args) {
        //get every model information from database and save them in array list
        Database.getInformation(new Manager());
        Database.getInformation(new Employee());
        Database.getInformation(new Passenger());
        Database.getInformation(new Airplane());
        Database.getInformation(new Flight());
        Database.getInformation(new Ticket());
        Database.getInformation(new TicketsAndPassengers());
        //set all lists
        setTicketsAndPassengers();
        setFlights();
        setPassengers();
        setTickets();

        //read all messages from file and save them in array list
        try {
            MessagePageController.readFileEmployee();
            MessagePageController.readFilePassenger();
        } catch (IOException e) {
            e.printStackTrace();
        }


        launch(args);

    }


}
